<?php
$db_host = "localhost";

$db_name = "orderingSystem";
$db_user = "root";
$db_pass = "";

//$db_name = "orderingsystem";
//$db_user = "rebeladmin";
//$db_pass = "r3b3l4dm1n!@#";

date_default_timezone_set('Asia/Manila');

try{
    $db_con = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_pass);
    $db_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e){
    echo $e->getMessage();
}
