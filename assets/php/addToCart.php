<?php
require_once 'DBconfig.php';

function addToCart()
{

    $productID = $_REQUEST['id'];
    $quantity = $_REQUEST['qty'];
    $tableNumber = $_REQUEST['tableNum'];

    global $db_con;

    try {
        $stmt = $db_con->query("INSERT INTO cart (productId,tableHash,quantity,status) VALUES ($productID,'$tableNumber',$quantity,'active')");
        //$orderDetails = $db_con->query("INSERT INTO orderDetails (productId,tableHash,quantity,status) VALUES ($productID,'$tableNumber',$quantity,1)");
        $updateProductQty = $db_con->query("UPDATE products SET quantity = quantity - $quantity WHERE id = $productID");
        $stmt2 = $db_con->query("SELECT *,SUM(quantity) as totalQty FROM cart WHERE tableHash = '$tableNumber' && status = 'active'");
        $row = $stmt2->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($row[0]['totalQty']);

    } catch (PDOException $e) {
        echo $e->getMessage();
    }

}

function cartCount()
{

    $tableNumber = $_REQUEST['tableNum'];

    global $db_con;

    try {
        $stmt = $db_con->query("SELECT *,SUM(quantity) as totalQty FROM cart WHERE tableHash = '$tableNumber' && status = 'active'");
        //echo $stmt2->rowCount();
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($row[0]['totalQty'] <= 0) {
            echo 0;
        } else {
            echo json_encode(array('qty' => $row[0]['totalQty'], 'id' => $row[0]['id']));
        }

    } catch (PDOException $e) {
        echo $e->getMessage();
    }

}

function getProductQty()
{

    $productID = $_REQUEST['id'];

    global $db_con;

    try {
        $stmt = $db_con->query("SELECT quantity FROM products WHERE id = $productID");
        $row = $stmt->fetch(PDO::FETCH_COLUMN);
        echo($row);

    } catch (PDOException $e) {
        echo $e->getMessage();
    }

}

$action = $_REQUEST['action'];
if ($action == 'addToCart') {
    addToCart();
} else if ($action == 'cartCount') {
    cartCount();
} else if ($action == 'getProductQty') {
    getProductQty();
}
