<?php
require_once 'DBconfig.php';

function confirmTableHash()
{
    global $db_con;
    $tableHash = $_REQUEST['tableHash'];

    if (strlen($tableHash) <= 0) {
        $tableHash = 0;
    }

    try {
        $stmt = $db_con->query("SELECT * FROM tables WHERE hash = '$tableHash'");
        $row = $stmt->rowCount();
        echo json_encode($row);

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function getCart()
{
    global $db_con;
    $tableHash = $_REQUEST['tableHash'];

    if (strlen($tableHash) <= 0) {
        $tableHash = 0;
    }

    try {
        $stmt = $db_con->query("SELECT *,SUM(c.quantity) as totalQty, c.id as cartId FROM cart as c INNER JOIN products as p
                                ON c.productID = p.id WHERE c.tableHash = '$tableHash' GROUP BY c.productId");
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($row);

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function grandTotal()
{
    global $db_con;
    $tableHash = $_REQUEST['tableNum'];

    if (strlen($tableHash) <= 0) {
        $tableHash = 0;
    }

    try {
        $grandTotal = 0;
        $stmt = $db_con->query("SELECT c.quantity,p.price FROM cart as c INNER JOIN products as p 
                                ON c.productID = p.id WHERE c.tableHash = '$tableHash'");
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($row as $key => $value) {
            $total = $row[$key]['quantity'] * $row[$key]['price'];
            $grandTotal += $total;
        }
        echo json_encode($grandTotal);

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function placeOrder()
{
    global $db_con;
    $daytime = (new \DateTime())->format('Y-m-d H:i:s');
    $tableHash = $_REQUEST['tableNum'];

    try {
        $grandTotal = 0;

        // select all data on orderDetails
        $stmt = $db_con->query("SELECT o.id,o.productId,o.tableHash,t.id as tableId,p.price,o.status,SUM(o.quantity) as totalQty FROM orderDetails as o
                                INNER JOIN products as p ON o.productID = p.id INNER JOIN tables as t ON o.tableHash = t.hash
                                WHERE o.tableHash = '$tableHash' GROUP BY o.productId");
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

//        foreach($row as $key1 => $value1){
//            $orderDetails = $db_con->query("INSERT INTO orderDetails (productId,tableHash,quantity,status) VALUES ($productID,'$tableNumber',$quantity,'active')");
//        }

        // grand total query
        $stmt2 = $db_con->query("SELECT c.quantity,p.price FROM cart as c INNER JOIN products as p 
                                ON c.productID = p.id WHERE c.tableHash = '$tableHash'");
        $row2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
        foreach ($row2 as $key => $value) {
            $total = $row2[$key]['quantity'] * $row2[$key]['price'];
            $grandTotal += $total;
        }

        $selectTable = $db_con->query("SELECT * FROM tables WHERE hash = '$tableHash'");
        $rowSelectTable = $selectTable->fetchAll(PDO::FETCH_ASSOC);

        foreach ($rowSelectTable as $key1 => $value1) {
            $tableId = $rowSelectTable[0]['id'];
        }

        // insert into orders
        $insertStmt = $db_con->query("INSERT INTO orders (tableId,status_id,grandTotal,order_time) VALUES ('$tableId',1,'$grandTotal','$daytime')");

        $selectOrder = $db_con->query("SELECT * FROM orders WHERE tableId = '$tableId' ORDER BY id DESC LIMIT 1");
        $rowSelectOrder = $selectOrder->fetchAll(PDO::FETCH_ASSOC);

        foreach ($rowSelectOrder as $key2 => $value2) {
            $orderId = $rowSelectOrder[0]['id'];
        }

        $selectCart = $db_con->query("SELECT * FROM cart WHERE tableHash = '$tableHash'");
        $rowSelectCart = $selectCart->fetchAll(PDO::FETCH_ASSOC);

        foreach ($rowSelectCart as $key => $value) {

            $cartProductid = $rowSelectCart[$key]['productId'];
            $cartTableHash = $rowSelectCart[$key]['tableHash'];
            $cartQuantity = $rowSelectCart[$key]['quantity'];

          $db_con->query("INSERT INTO orderdetails (productId,tableHash,quantity,status,orderid)
                                                  VALUES ('$cartProductid','$cartTableHash','$cartQuantity','1','$orderId')");
        }

            $stmtDeleteCart = $db_con->query("DELETE FROM cart WHERE tableHash = '$tableHash'");
            echo json_encode('Ordered');

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function deleteItem()
{
    global $db_con;
    $tableHash = $_REQUEST['tableNum'];
    $productId = $_REQUEST['productId'];
    $totalQty = $_REQUEST['totalQty'];

    if (strlen($tableHash) <= 0) {
        $tableHash = 0;
    }

    try {
        $stmt = $db_con->query("DELETE FROM cart WHERE productId = $productId && tableHash = '$tableHash'");
        $stmt = $db_con->query("DELETE FROM orderdetails WHERE productId = $productId && tableHash = '$tableHash'");
        $updateProductQty = $db_con->query("UPDATE products SET quantity = quantity + $totalQty WHERE id = $productId");

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

$action = $_REQUEST['action'];
if ($action == 'confirmTableHash') {
    confirmTableHash();
} else if ($action == 'getCart') {
    getCart();
} else if ($action == 'grandTotal') {
    grandTotal();
} else if ($action == 'placeOrder') {
    placeOrder();
} else if ($action == 'deleteItem') {
    deleteItem();
}
