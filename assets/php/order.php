<?php
require_once 'DBconfig.php';

function confirmTableHash()
{
    global $db_con;
    $tableHash = $_REQUEST['tableHash'];

    if (strlen($tableHash) <= 0) {
        $tableHash = 0;
    }

    try {
        $stmt = $db_con->query("SELECT * FROM tables WHERE hash = '$tableHash'");
        $row = $stmt->rowCount();
        echo json_encode($row);

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}


function getOrder()
{
    global $db_con;
    $tableHash = $_REQUEST['tableHash'];

    if (strlen($tableHash) <= 0) {
        $tableHash = 0;
    }

    try {
        $stmt = $db_con->query("SELECT *,SUM(c.quantity) as totalQty, c.status as itemStatus
                                FROM orderdetails as c 
                                INNER JOIN products as p ON c.productID = p.id 
                                INNER JOIN orders as o ON c.orderid = o.id
                                INNER JOIN orderstatus as os ON os.id = c.status
                                WHERE c.tableHash = '$tableHash' GROUP BY c.orderid,p.id");
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($row);

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function grandTotal()
{
    global $db_con;
    $tableHash = $_REQUEST['tableNum'];

    if (strlen($tableHash) <= 0) {
        $tableHash = 0;
    }

    try {
        $grandTotal = 0;
        $stmt = $db_con->query("SELECT c.quantity,c.orderid,p.price,o.status_id,c.status FROM orderdetails as c 
                                INNER JOIN products as p ON c.productID = p.id 
                                INNER JOIN orders as o ON c.orderid = o.id
                                WHERE c.tableHash = '$tableHash'  && o.status_id IN (1,2,3) && c.status IN (1,2,3)");
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($row as $key => $value) {
            $total = $row[$key]['quantity'] * $row[$key]['price'];
            $grandTotal += $total;
        }
        echo json_encode($grandTotal);

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function orderStatus()
{
    global $db_con;
    $tableHash = $_REQUEST['tableNum'];

    if (strlen($tableHash) <= 0) {
        $tableHash = 0;
    }

    try {

        $stmt = $db_con->query("SELECT os.id,os.status_name FROM orders as o INNER JOIN orderStatus as os ON
                                o.status_id = os.id WHERE o.tableId = '$tableHash'");
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode(array('id'=>$row[0]['id'],'status'=>$row[0]['status_name']));

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}


$action = $_REQUEST['action'];
if ($action == 'confirmTableHash') {
    confirmTableHash();
} else if ($action == 'getOrder') {
    getOrder();
} else if ($action == 'grandTotal') {
    grandTotal();
} else if ($action == 'orderStatus') {
    orderStatus();
}
