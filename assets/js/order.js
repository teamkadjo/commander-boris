$(function () {

    // domain name
    var domain = window.location.hostname;

    //query string
    params = new URL(document.URL).searchParams;
    var tableNumber = params.get('table');

    // number format commas
    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

    // confirms table hash
    $.ajax({
        type: "POST",
        dataType: 'json',
        data: {
            tableHash: tableNumber
        },
        url: "assets/php/order.php?action=confirmTableHash",
        success: function (data) {

            var params2 = params.toString().length;

            //confirming table hash if valid
            if (data == 0) {
                $(".order-section").hide();
                swal({
                    title: 'Whoops!',
                    text: "No content for you here!",
                    type: 'warning',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK',
                    timer: 4000,
                    allowOutsideClick: false
                }).then(
                    function () {
                        swal({
                            title: 'Redirecting...',
                            timer: 2000,
                            type: 'info',
                            showConfirmButton: false
                        }).then(
                            function () {
                            },
                            function (dismiss) {
                                if (dismiss === 'timer') {
                                    window.location = 'http://' + domain;
                                }
                            }
                        )
                    }, function (dismiss) {
                        if (dismiss) {
                            swal({
                                title: 'Redirecting...',
                                timer: 2000,
                                type: 'info',
                                showConfirmButton: false
                            }).then(
                                function () {
                                },
                                function (dismiss) {
                                    if (dismiss === 'timer') {
                                        window.location = 'http://' + domain;
                                    }
                                }
                            )
                        }
                    });
            } else {
//get all items from cart

                function getAllOrder() {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        data: {
                            tableHash: tableNumber
                        },
                        async: false,
                        url: "assets/php/order.php?action=getOrder",
                        success: function (data) {

                            var qwe = 0;

                            $.each(data, function (index, value) {

                                var orderedItems = $("<div class='row'>" +
                                    "<div class='col-md-12'><h1 id='" + value.orderid + "'></h1></div><br><br>" +
                                    "<div class='col-xs-2'><img class='img-responsive' src='admin/" + value.productImage + "' width='150'> </div>" +
                                    "<div class='col-xs-4'> " +
                                    "<h4 class='product-name'><strong>" + value.name + "</strong></h4>" +
                                    "<h4><small>" + value.description + "</small></h4>" +
                                    "<span class='" + value.status_name + "_servingTime'>Estimated time: </span><label class='" + value.status_name + "_servingTime" + qwe++ + "'>" + value.servingtime + " </label>" +
                                    "<div class='progress'>" +
                                    "<div class='progress-bar' " +
                                    "role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100'" +
                                    "style='width: 100%'><span> " + value.status_name + " </span></div> </div>" +
                                    "</div>" +
                                    "<div class='col-xs-6'>" +
                                    "<div class='col-xs-6 text-right'>" +
                                    "<h6><strong>" + value.price + "<span class='text-muted'>x</span></strong></h6>" +
                                    "</div>" +
                                    "<div class='col-xs-4'>" +
                                    "<input type='text' class='form-control input-sm' value='" + value.totalQty + "' disabled>" +
                                    "</div>" +

                                    "</div>" +
                                    "  </div>"
                                );

                                $("#orderedItems").append(orderedItems);

                                $('#' + value.orderid + ':eq(0)').text('ORDER #');

                                $('.progress-bar span:contains("Being Prepared")').parent().addClass('progress-bar-info progress-bar-striped  active');
                                $('.progress-bar span:contains("Served")').parent().addClass('progress-bar-primary progress-bar-striped  active');
                                $('.progress-bar span:contains("Paid")').parent().addClass('progress-bar-success progress-bar-striped');
                                $('.progress-bar span:contains("Void")').parent().addClass('progress-bar-danger progress-bar-striped');

                            });

                            var x = ($('h1:contains("ORDER #")').size());
                            var y = 1;
                            var tae = qwe;

                            $('#hiddenValue').val(tae);


                            $('span.Served_servingTime').hide();
                            $('span.Paid_servingTime').hide();
                            $('span.Void_servingTime').hide();

                            for (var iqwe = 0; iqwe < tae; iqwe++) {

                                $('label.Served_servingTime' + iqwe).hide();
                                $('label.Paid_servingTime' + iqwe).hide();
                                $('label.Void_servingTime' + iqwe).hide();

                                $('label.Being.Prepared_servingTime' + iqwe).html($('label.Being.Prepared_servingTime' + iqwe).text().replace(' minutes', ':00'));

                            }

                            for (var i = 0; i < x; i++) {
                                $('h1:contains("ORDER #"):eq(' + i + ')').append(y++);
                            }

                            $('h1:contains("ORDER #")').prepend('<hr>');
                            $('h1:contains("ORDER #")').append('<br><br>');
                            $('h1:contains("ORDER #"):eq(0) hr').remove();

                        }
                    });
                }

                getAllOrder();

                //countdown
                var hiddenValue = $('#hiddenValue').val();

                var interval = setInterval(function () {

                    for (var xyz = 0; xyz < hiddenValue; xyz++) {

                        var timer = $('label.Being.Prepared_servingTime' + xyz).text().split(':');
                        var minutes = parseInt(timer[0], 10);
                        var seconds = parseInt(timer[1], 10);
                        seconds -= 1;
                        if (minutes < 0) return clearInterval(interval);
                        if (minutes < 10 && minutes.length != 2) minutes = '0' + minutes;
                        if (seconds < 0 && minutes != 0) {
                            minutes -= 1;
                            seconds = 59;
                        }
                        else if (seconds < 10 && length.seconds != 2) seconds = '0' + seconds;
                        $('label.Being.Prepared_servingTime' + xyz).html(minutes + ':' + seconds);

                        if (minutes == 0 && seconds == 0)
                            clearInterval(interval);

                        if ($('label.Being.Prepared_servingTime' + xyz).html() == '00:00') {
                            $('label.Being.Prepared_servingTime' + xyz).html($('label.Being.Prepared_servingTime' + xyz).html().replace('00:00', 'To be serve any minute now!'));
                        }

                    }


                }, 1000);

                //grand total
                function grandTotal() {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        data: {
                            tableNum: tableNumber
                        },
                        url: "assets/php/order.php?action=grandTotal",
                        success: function (data) {
                            $("#grandTotal").append("₱" + numberWithCommas(data));
                        }
                    });
                }

                grandTotal();

                // setInterval(function () {
                //     $("#orderedItems").empty();
                //     $("#grandTotal").empty();
                //     getAllOrder();
                //     grandTotal();
                // }, 5000);

                //order status
                // var statusId = 0;
                // var loadingStatusId = 0;
                //
                // function orderStatus() {
                //     $.ajax({
                //         type: "POST",
                //         dataType: 'json',
                //         data: {
                //             tableNum: tableNumber
                //         },
                //         url: "assets/php/order.php?action=orderStatus",
                //         success: function (data) {
                //             statusId = data.id;
                //             $("#orderStatus").append(data.status);
                //             if (data.id == 1) {
                //                 $('.progress-bar').addClass('progress-bar-info');
                //                 $('.progress-bar').addClass('active');
                //
                //             } else if (data.id == 2) {
                //                 $('.progress-bar').addClass('progress-bar-primary');
                //                 $('.progress-bar').addClass('active');
                //             } else {
                //                 $('.progress-bar').addClass('progress-bar-success');
                //                 $('.progress-bar').removeClass('active');
                //             }
                //         }
                //     });
                // }
                //
                //  orderStatus();
                // setInterval(function () {
                //     $.ajax({
                //         type: "POST",
                //         dataType: 'json',
                //         data: {
                //             tableNum: tableNumber
                //         },
                //         url: "assets/php/order.php?action=orderStatus",
                //         success: function (data) {
                //             loadingStatusId = data.id;
                //             if (statusId != loadingStatusId) {
                //                 orderStatus();
                //                 $("#orderStatus").empty();
                //                 $('.progress-bar').removeClass('progress-bar-info');
                //                 $('.progress-bar').removeClass('progress-bar-primary');
                //                 $('.progress-bar').removeClass('progress-bar-success');
                //             }
                //         }
                //     });
                // }, 5000);
            }
        }
    });


});