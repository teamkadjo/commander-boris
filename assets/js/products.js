$(function () {

    //change active tabs
    $("#nav-mealCourse li a").click(function () {
        $("#tab-content-mealCourse .tab-pane").removeClass('active');
        var href = $(this).attr('href');
        $(href.substring(1)).addClass('active');
    });

    $.ajax({
        type: "POST",
        url: "assets/php/products.php?action=getMealCourse",
        dataType: 'json',
        success: function (data) {

            $.each(data, function (index, value) {
                var mealCouse = value.mealCourse.replace(/\s+/g, '');

                var mealCourse = $("<li>" +
                    "<a data-toggle='tab' role='tab' href='#" + mealCouse + "-tab'> " + value.mealCourse + " </a>" +
                    "</li>");

                var mealCourseTab = $("<div id='" + mealCouse + "-tab' class='tab-pane fade in' role='tabpanel'>" +
                    "<div class='row' id='" + mealCouse + "'></div>" +
                    "</div>");

                //meal course titles
                $("#nav-mealCourse").append(mealCourse);
                $("#nav-mealCourse li:first-child").addClass('active');

                //tabs of meal course
                $("#tab-content-mealCourse").append(mealCourseTab);
                $("#tab-content-mealCourse .tab-pane:first-child").addClass('active');

            });

        }
    });

    $.ajax({
        type: "POST",
        url: "assets/php/products.php?action=getProducts",
        dataType: 'json',
        success: function (data) {

            $.each(data, function (index, value) {

                var mealCourse = value.mealCourse.replace(/\s+/g, '');

                var products = $("<div class='col-md-6 col-sm-12 col-xs-12'>" +
                    "<div class='item'>" +
                    "<div class='img'>" +
                    "<img src='admin/" + value.productImage + "' alt='admin/" + value.productImage + "' width='110' height='110'>" +
                    "</div>" +
                    "<div class='details'>" +
                    "<h3><a href='single-item.html'> " + value.name + " </a></h3>" +
                    "<p>" + value.name + " <br> " + value.description + " </p>" +
                    "</div>" +
                    "<div class='price'>" +
                    "<span>" + value.price + "</span>" +
                    "</div>" +
                    "</div>");

                $("#" + mealCourse).append(products);

            });
        }
    });

});