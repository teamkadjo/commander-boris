$(function () {

    $.ajax({
        type: "POST",
        url: "assets/php/testproducts.php?action=getProducts",
        dataType: 'json',
        beforeSend: function(jqXHR, settings) {
            console.log(settings.url);
        },
        success: function (data) {

            $.each(data, function( index, value ) {
                var row = $("<tr><td>" + value.id + "</td><td>" + value.name + "</td>" +
                    "<td>" + value.description + "</td> <td>" + value.price + "</td></tr>");
                $("#tbodyProducts").append(row);
            });
        }
    });

    $.ajax({
        type: "POST",
        url: "assets/php/testproducts.php?action=getUsers",
        dataType: 'json',
        beforeSend: function(jqXHR, settings) {
            console.log(settings.url);
        },
        success: function (data) {
            $.each(data, function( index, value ) {
                var row = $("<tr><td>" + value.id + "</td><td>" + value.username + "</td>" +
                    "<td>" + value.password + "</td> <td>" + value.role + "</td></tr>");
                $("#tbodyUsers").append(row);
            });
        }
    });

});