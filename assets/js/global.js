$(function () {

    //query string
    params = new URL(document.URL).searchParams;
    var tableNumber = params.get('table');

    //domain name for redirecting index
    var domain = window.location.hostname;

    // no cart item yet
    function alertNoCart() {
        $('.single-item').hide();
        swal({
            title: 'No item',
            text: "No item in cart yet!",
            type: 'warning',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            timer: 1500,
            allowOutsideClick: false
        })
    }

    if (tableNumber == null) {
        $('.indexLink').attr({
            href: 'http://' + domain
        });
        $("li.cart").hide(); // hide cart button
    } else {
        $('.indexLink').attr({
            href: 'http://' + domain + '/?table=' + tableNumber
        });
    }

    //change active tabs
    $("#nav-mealCourse li a").click(function () {
        $("#tab-content-mealCourse .tab-pane").removeClass('active');
        var href = $(this).attr('href');
        $(href.substring(1)).addClass('active');
    });

    //cart count onClick
    $("#cartBtn,.viewOrder").click(function () {
        $.ajax({
            type: "POST",
            dataType: 'json',
            data: {
                tableNum: tableNumber
            },
            url: "assets/php/addToCart.php?action=cartCount",
            success: function (data) {

                // check if tableNumber has item on cart
                if (data.qty == null) {
                    var totalQty = data.qty = 0;
                    $("#cartCount").empty();
                    $("#cartCount").append(totalQty);
                    alertNoCart();
                } else {
                    var totalQty = data.qty;
                    window.location = "cart.html?table=" + tableNumber;
                    $("#cartCount").empty();
                    $("#cartCount").append(totalQty);
                }

            }
        });
    });

    //cart count
    $.ajax({
        type: "POST",
        dataType: 'json',
        data: {
            tableNum: tableNumber
        },
        url: "assets/php/addToCart.php?action=cartCount",
        success: function (data) {

            if (data.qty == null) {
                var totalQty = data.qty = 0;
            } else {
                var totalQty = data.qty;
            }
            $("#cartCount,.cart-count").empty();
            $("#cartCount,.cart-count").append(totalQty);
        }
    });

    // active top navigation
    $('.navbar-nav.navbar-right li a').click(function () {
        $('.navbar-nav.navbar-right li a').removeClass('active');
        $(this).addClass('active');
    });

    // smooth scroll MENU
    $(".menu-nav").click(function () {
        $('html, body').animate({
            scrollTop: $("#recipes").offset().top
        }, 1000);
    });

});