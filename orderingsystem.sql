/*
Navicat MySQL Data Transfer

Source Server         : Restorante
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : orderingsystem

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-03-15 15:35:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `productId` int(50) NOT NULL,
  `tableHash` varchar(50) NOT NULL,
  `quantity` int(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cart
-- ----------------------------

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `date` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of feedback
-- ----------------------------
INSERT INTO `feedback` VALUES ('1', 'Ed', 'ed@tae.com', 'Ed tae', 'Ed loso', '2017-03-09 08:40:56');

-- ----------------------------
-- Table structure for inventory
-- ----------------------------
DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stocknum` int(11) DEFAULT NULL,
  `productcode` varchar(100) DEFAULT NULL,
  `description` text,
  `quantity` int(11) DEFAULT NULL,
  `supplier` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of inventory
-- ----------------------------
INSERT INTO `inventory` VALUES ('1', '0', 'sdkjhfskjdhf', 'jksdhfkjshd', '12', 'skdflksdf');
INSERT INTO `inventory` VALUES ('2', '0', 'sdfsdf', 'sdfsdf', '22132', 'sdfsdf');
INSERT INTO `inventory` VALUES ('3', '0', 'ljkjlj', 'khkhkj', '675', 'hjgjhg');

-- ----------------------------
-- Table structure for orderdetails
-- ----------------------------
DROP TABLE IF EXISTS `orderdetails`;
CREATE TABLE `orderdetails` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `productId` int(50) NOT NULL,
  `tableHash` varchar(50) NOT NULL,
  `quantity` int(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `status` varchar(50) NOT NULL,
  `orderid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of orderdetails
-- ----------------------------
INSERT INTO `orderdetails` VALUES ('1', '25', '123', '2', '', '2', '1');
INSERT INTO `orderdetails` VALUES ('2', '1', '123', '1', '', '1', '1');
INSERT INTO `orderdetails` VALUES ('3', '1', '123', '2', '', '1', '2');
INSERT INTO `orderdetails` VALUES ('4', '5', '123', '3', '', '1', '2');
INSERT INTO `orderdetails` VALUES ('5', '25', '123', '1', '', '1', '3');
INSERT INTO `orderdetails` VALUES ('6', '7', '123', '1', '', '1', '4');
INSERT INTO `orderdetails` VALUES ('7', '25', 'zDhuSAirsY', '2', '', '1', '5');
INSERT INTO `orderdetails` VALUES ('8', '1', 'zDhuSAirsY', '1', '', '1', '5');
INSERT INTO `orderdetails` VALUES ('9', '9', 'zDhuSAirsY', '1', '', '1', '6');
INSERT INTO `orderdetails` VALUES ('10', '11', 'zDhuSAirsY', '2', '', '1', '6');
INSERT INTO `orderdetails` VALUES ('11', '16', 'zDhuSAirsY', '2', '', '1', '7');
INSERT INTO `orderdetails` VALUES ('12', '18', 'zDhuSAirsY', '2', '', '1', '7');
INSERT INTO `orderdetails` VALUES ('13', '25', '123', '2', '', '1', '8');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `tableId` varchar(100) NOT NULL,
  `status_id` int(50) NOT NULL,
  `grandTotal` int(50) NOT NULL,
  `order_time` varchar(50) NOT NULL,
  `real_tableid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('1', '1', '2', '1600', '2017-03-09 02:53:49', '1');
INSERT INTO `orders` VALUES ('2', '3', '1', '997', '2017-03-09 06:34:58', '5');
INSERT INTO `orders` VALUES ('3', '5', '1', '700', '2017-03-09 06:38:36', '1');
INSERT INTO `orders` VALUES ('4', '123', '3', '149', '2017-03-09 06:44:53', '2');
INSERT INTO `orders` VALUES ('5', '3', '1', '1600', '2017-03-09 08:04:43', '0');
INSERT INTO `orders` VALUES ('6', '3', '1', '447', '2017-03-09 08:05:18', '0');
INSERT INTO `orders` VALUES ('7', '4', '3', '976', '2017-03-09 08:27:23', null);
INSERT INTO `orders` VALUES ('8', '1', '1', '1400', '2017-03-15 10:31:59', null);

-- ----------------------------
-- Table structure for orderstatus
-- ----------------------------
DROP TABLE IF EXISTS `orderstatus`;
CREATE TABLE `orderstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of orderstatus
-- ----------------------------
INSERT INTO `orderstatus` VALUES ('1', 'Being Prepared');
INSERT INTO `orderstatus` VALUES ('2', 'Served');
INSERT INTO `orderstatus` VALUES ('3', 'Paid');
INSERT INTO `orderstatus` VALUES ('4', 'Void');

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_amount` double DEFAULT NULL,
  `cash` double DEFAULT NULL,
  `datemodified` datetime DEFAULT NULL,
  `dateposted` datetime DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of payment
-- ----------------------------
INSERT INTO `payment` VALUES ('1', '149', '149', '2017-03-08 23:50:44', '2017-03-08 23:50:44', '4');
INSERT INTO `payment` VALUES ('2', '976', '976', '2017-03-09 00:41:01', '2017-03-09 00:41:01', '7');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `price` varchar(250) NOT NULL,
  `mealCourse` varchar(100) NOT NULL,
  `servings` int(11) NOT NULL,
  `productImage` varchar(500) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `servingtime` varchar(50) DEFAULT NULL,
  `expdate` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', 'Kani Stick Fry Curry', 'test description', '200', 'Original Curry', '1', 'assets\\images\\products\\kaniStick.jpg', '96', 'active', '5 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('2', 'Chicken Fry Curry', 'test description', '199', 'Original Curry', '1', 'assets\\images\\products\\chickenFryCurry.jpg', '100', 'active', '10 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('3', 'Ton Katsu Curry', 'test description', '199', 'Original Curry', '1', 'assets\\images\\products\\tonKatsuCurry.jpg', '100', 'active', '15 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('4', 'Korroke Curry', 'test description', '199', 'Original Curry', '1', 'assets\\images\\products\\korrokeCurry.jpg', '100', 'active', '20 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('5', 'Menchi Katsu Curry', 'test description', '199', 'Original Curry', '1', 'assets\\images\\products\\menchiCurry.JPG', '97', 'active', '25 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('6', 'Extra Ring Fry Curry', 'test description', '199', 'Original Curry', '1', 'assets\\images\\products\\extraRingFryCurry.jpg', '100', 'active', '30 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('7', 'Vegetable Tempura', 'test description', '149', 'Tempura', '1', 'assets\\images\\products\\vegetableTempura.jpg', '99', 'active', '35 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('8', 'Tempura Moriawase', 'test description', '399', 'Tempura', '1', 'assets\\images\\products\\tempuraMoriawase.jpg', '100', 'active', '40 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('9', 'Ebi Tempura', 'test description', '149', 'Tempura', '1', 'assets\\images\\products\\ebiTempura.jpg', '99', 'active', '45 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('10', 'Jumbo Ebi Tempura', 'test description', '249', 'Tempura', '1', 'assets\\images\\products\\jumboEbiTempura.jpg', '100', 'active', '50 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('11', 'Ebi Ika Kakaigi', 'test description', '149', 'Tempura', '1', 'assets\\images\\products\\ebiIkaKakaigi.jpg', '98', 'active', '15 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('12', 'Squid Tempura', 'test description', '149', 'Tempura', '1', 'assets\\images\\products\\squidTempura.jpg', '100', 'active', '10 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('13', 'Seafood Ramen', 'test description', '199', 'Ramen', '1', 'assets\\images\\products\\seafoodRamen.jpg', '100', 'active', '5 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('14', 'Corn Ramen', 'test description', '249', 'Ramen', '1', 'assets\\images\\products\\cornRamen.JPG', '100', 'active', '20 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('15', 'Buta Ramen', 'test description', '249', 'Ramen', '1', 'assets\\images\\products\\butaRamen.jpg', '100', 'active', '30 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('16', 'Kani Tama Ramen', 'test description', '249', 'Ramen', '1', 'assets\\images\\products\\kaniTamaRamen.jpg', '98', 'active', '35 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('17', 'Tempura Ramen', 'test description', '249', 'Ramen', '1', 'assets\\images\\products\\tempuraRamen.jpg', '100', 'active', '25 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('18', 'Hiyachi Cold Ramen', 'test description', '239', 'Ramen', '1', 'assets\\images\\products\\hiyachiColdRamen.jpg', '98', 'active', '10 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('25', 'Red label johnnie walker ', 'whiskey ', '700', 'Drinks ', '8', 'assets\\images\\products\\img-the-bar-collection_0064_johnnie-walker-red.png', '93', 'active', '40 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('26', 'black label johnnie walker', 'whiskey ', '1700', 'Drinks ', '5', 'assets\\images\\products\\JWalker black.png', '100', 'active', '45 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('27', 'Double Black Label Johnnie Walker', 'whiskey', '6000', 'Drinks', '4', 'assets\\images\\products\\JW double Black.png', '100', 'active', '55 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('28', 'Gold Label johnnie Walker', 'whiskey', '2400', 'Drinks', '5', 'assets\\images\\products\\Gold-label-reserve2.png', '100', 'active', '10 minutes', '03/01/2017');
INSERT INTO `products` VALUES ('29', 'Product 1', 'Lami ni siya', '10.00', 'Original Curry', '5', 'assets\\images\\products\\51DRCYVG8xL._SY300_.jpg', '100', 'active', '10 minutes', '03/01/2017');

-- ----------------------------
-- Table structure for tables
-- ----------------------------
DROP TABLE IF EXISTS `tables`;
CREATE TABLE `tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tablename` varchar(250) NOT NULL,
  `hash` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tables
-- ----------------------------
INSERT INTO `tables` VALUES ('1', 'One', '123');
INSERT INTO `tables` VALUES ('2', 'Two', 'TNmkchkozf');
INSERT INTO `tables` VALUES ('3', 'Three', 'zDhuSAirsY');
INSERT INTO `tables` VALUES ('4', 'Four', 'PGNPlygVYj');
INSERT INTO `tables` VALUES ('5', 'Take-Out', 'b7O8yxs5ls');
INSERT INTO `tables` VALUES ('6', 'test', '');
INSERT INTO `tables` VALUES ('7', 'test1', 'wKwgFuJNsM');
INSERT INTO `tables` VALUES ('8', 'test2', 'X92AsV0YV5');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', 'admin', 'admin', 'Admin User');
INSERT INTO `users` VALUES ('2', 'cashier', 'cashier', 'cashier', 'cashier');
INSERT INTO `users` VALUES ('3', 'cook', 'cook', 'cook', 'cook');
INSERT INTO `users` VALUES ('4', 'john', 'hhhhh', 'admin', null);
INSERT INTO `users` VALUES ('6', 'john', 'john72', 'cook', null);
INSERT INTO `users` VALUES ('8', 'Tintin', 'password', 'Lagay', null);
