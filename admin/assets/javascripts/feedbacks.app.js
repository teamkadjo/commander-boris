function loadTable(){
$('#feedbacks tbody').html("");
		    $.ajax({
				        type: "POST",
				        url: "assets/php/feedbacks.handler.php?funct=viewprod",
				        dataType: 'json',
				        success: function (data) {
						var tbl = $('#feedbacks').DataTable();
						$('#feedbacks_wrapper .table-caption').text('Feedbacks Table');
						$('#feedbacks_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
						tbl.clear();
				            $.each(data, function (index, value) {
				                var row = $("<tr>" +
												"<td>" + value.id + "</td>" +
												"<td>" + value.name + "</td>" +
												"<td>" + value.email + "</td>" +
												"<td>" + value.subject + "</td>" +
												"<td>" + value.message	+ "</td>" +
												"<td>" + value.date + "</td>" +
												
											"</tr>");
				                tbl.row.add(row).draw(true);


				            });
				        }
			});
	}