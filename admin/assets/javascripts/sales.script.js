	var dtable = null;
	
	
	function updateTableListings() {

				$.ajax({url:'assets/php/sales.handler.php',dataType:'html',data:{action:'get-table-list'},success:function(data){
					$("#table_listing tbody").html(data);
					//dtable._fnDraw();

				}});	
	
	}


	function getUrlVars()
	{
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for(var i = 0; i < hashes.length; i++)
		{
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	}

	function displayDetails(id){
		console.log("Displaying " + id);
	
	}

	function displayReceipt(id){
		myWindow=window.open('/admin/print-receipt.html?id='+id,'','width=500,height=600');
		myWindow.document.close(); //missing code
		myWindow.focus();	
	}

   // from http://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-money-in-javascript
	function format2(n, currency) {
		return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	}
	

	
	function updateAmount(){
		
		var totalprice = $("#totalprice_display").text()
				
		totalprice = totalprice.replace(",","");
		the_price = parseFloat($("#cashonhand").val()) - parseFloat(totalprice );
		$("#change_amount").text(format2(the_price,"")); 
	}
	
function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}	

	
	init.push(function () {

			jQuery(".optionRadios").on("click",function(){
				if(jQuery(this).val() == "cash") {
					jQuery(".credit").hide();
					jQuery(".cash").show();
				}
				else 
				if(jQuery(this).val() == "credit") {
					jQuery(".cash").hide();
					jQuery(".credit").show();
				}
			});
		
		
		/*  order window dialog related scripts  */
		
						$.ajax({url:'assets/php/sales.handler.php',data:{action:'get-tables'},dataType:'json',success:function(data){
					$("#table_number_selection").html("");
					$.each(data,function(index,value){
						
						$("#table_number_selection").append($('<option>',
							 {
								value: value.id,
								text : value.tablename 
							}));
					});
					
				}});
				
				
				$("#close_order_dialog").on('click',function(){					
						$('.modal-backdrop').hide(); // for black background
						$('body').removeClass('modal-open'); // For scroll run
						$("#modal-sizes-1").hide();	
						location.reload();
					});


				$("#close_pay_dialog").on('click',function(){					
						$('.modal-backdrop').hide(); // for black background
						$('body').removeClass('modal-open'); // For scroll run
						$("#modal_payment").hide();	
					});

				$("#update_order").on('click',function(){
					
					$.ajax({url:'assets/php/sales.handler.php',data:{action:'update-Order',tableid:$("#table_number_selection").val(),statusid:$("#order_status_selection").val(),orderid:$("#order_id_modal").text()},dataType:'json',success:function(data){
						
						updateTableListings();
						location.reload();
						
						
						}}) 
					
					});
				
				

				
		/* end of order window dialog related scripts */
		
		
				updateTableListings();

		
		
					$('#ui-bootbox-confirm').on('click', function () {
							bootbox.confirm({
								message: "Are you sure you want to void this Order : <strong>#" + $("#order_id").text() +" </strong> ?",
								callback: function(result) {
									alert("Confirm result: " + result);
								},
								className: "bootbox-sm"
							});
						});
						
					//$("body").addClass("mmc");
					
					
					$("#table_listing tbody").on('click','tr',function(){
						var order_id1 = $(this).find("td:eq(2)").text()
						$("#order_id").text(order_id1);
						$("#order_id_modal").text(order_id1);
						
						jQuery.ajax({url:'assets/php/sales.handler.php',dataType:'html',data:{action:'get-order-details',order_id:order_id1},success:function(data){
						$("#order_details tbody").html(data);
						$("#order_details tr.editable").attr("data-toggle","modal");
						$("#order_details tr.editable").attr("data-target","#edit_item");
						$("#totalprice_display").text($("#total_amount_2pay").text());
						
						
						$('#edit_item').on('shown.bs.modal', function (e) {
							var id = $(e.relatedTarget).data('id'); //<- It's data-id value							
						});
						
				}});
						
					});
					
					

		
					
					
//					$("#order_details tr.editable").attr("data-toggle","modal");
//					$("#order_details tr.editable").attr("data-target","#edit_item");
					



						
						

					
					$("#cashonhand").keydown(function (e) {
						// Allow: backspace, delete, tab, escape, enter and .
						if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
							 // Allow: Ctrl+A
							(e.keyCode == 65 && e.ctrlKey === true) || 
							 // Allow: home, end, left, right
							(e.keyCode >= 35 && e.keyCode <= 39)) {
								 // let it happen, don't do anything
								 return;
						}
						// Ensure that it is a number and stop the keypress
						if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
							e.preventDefault();
						}
						
					});	
					
				$("#cashonhand").change(function(){
					updateAmount();
				});

/* payment portion */
				
				$("#pay_now").on('click',function(){
	

							$("#modal_payment").hide();

					if (confirm("Confirm Payment for Order : <strong>#" + $("#order_id").text() +" </strong> ?")) {
									var order_id1 = $("#order_id").text(); 
									var totalprice = $("#totalprice_display").text();	
											totalprice = totalprice.replace(",","");
											the_price = parseFloat($("#cashonhand").val()) - parseFloat(totalprice );
											jQuery.ajax({url:'assets/php/sales.handler.php',type:'POST',dataType:'html',data:{paytype:jQuery(".optionRadios:checked").val(),action:'add-Payment',order_id:order_id1,cash:$("#cashonhand").val(),total_amount:totalprice,creditcard:jQuery("#creditcardnumber").val()},success:function(data){												
													updateTableListings();
													location.reload();
												}
												});

									
									
								}

					
				});
/* end of payment portion */			
				
				$("#paying_now_button").on('click',function(){
					$("#modal_payment").modal("show");
					$("#pay_now").show();					
				});
				

			dtable = 	    $('#jq-datatables-example').DataTable( {
							"processing": true,
							"serverSide": true,
							"ajax": "assets/php/sales.handler.php?action=get-Sales",
							"columns":[{"data":"time","fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
            $(nTd).html("<a href='javascript:displayDetails("+oData.id + ")'>" + oData.time + "</a>");}},{"data":"tablename"},{"data":"id","fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
            $(nTd).html("<a href='javascript:displayDetails("+oData.id + ")'>" + oData.id + "</a>");}},{"data":"status_name"},{"data":"grandTotal"},{"data":"clickid","fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
            	if (oData.status_name == "Paid" ) { $(nTd).html("<a class='btn btn-primary' href='javascript:displayReceipt("+oData.clickid + ")'> Print Invoice</a>");}
            	else {

            		$(nTd).html("");
            	}
           }}],
							"order": [[ 2, "desc" ]],
						} );



	})
	window.PixelAdmin.start(init);
