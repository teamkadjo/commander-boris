	


	init.push(function () {
			loadTables();

	})
	window.PixelAdmin.start(init);

	function loadTables(){

		$.ajax({

			type: "POST",
			url: "assets/php/hash.handler.php?funct=gettables",
			dataType: 'json',
			success: function (data) {
				            $.each(data, function (index, value) {

				         			$("#content-wrapper").append("<div class='col-md-6 genqr1' id='table-"+value.id+"'> " +
										"<div class='stat-panel' style='margin-bottom:0px !important'> " +
											"<div id='qrcode_"+value.id+"' style='width:100px; height:100px;' class='stat-cell bg-success bordered no-border-vr no-border-l no-padding valign-middle text-center text-lg'></div>" +
											"<div class='stat-cell col-xs-7 no-padding valign-middle'> " +
												"<div class='stat-rows'> " +
													"<div class='stat-row'> " +
														"<p class='stat-cell bg-success padding-sm valign-middle'> " +
															"Table: " + value.id  + " / Table Name: " + value.tablename +
															"<i class='fa fa-envelope-o pull-right'></i> " +
														"</p> " +
													"</div> " +
													"<div class='stat-row'> " +
														"<p class='stat-cell bg-success darken padding-sm valign-middle'> " +
															"Order Link:  http://restorante.rebel-it.tech/?table=" + value.hash +
															"<i class='fa fa-bug pull-right'></i> " +
														"</p> " +
													"</div> " +
													"<div class='stat-row'> " +
														"<a href='printqrcode.html?hash="+value.hash+"&tableid="+value.id+"' class='stat-cell bg-info darker padding-sm valign-middle'> " +
															"Print " +
															"<i class='fa fa-users pull-right'></i> " +
														"</a> " +
													"</div> " +
												"</div>  " +
											"</div>  " +
											
										"</div>  " +
										"<div class='stat-panel genqr2'> " +
											"<a class='btn btn-info col-md-12 generateqr' id='genqr_" + value.id + "'>Generate QR Code</a>" +
										"</div>" +		
									"</div>	");

									//generate QR Code:
									//new QRCode(document.getElementById("qrcode_"+value.id), "http://localhost/?table=" + value.hash);

									var qrcode = new QRCode(document.getElementById("qrcode_"+value.id), {
										text: "http://restorante.rebel-it.tech/?table=" + value.hash,
										width: 100,
										height: 100,
										colorDark : "#000000",
										colorLight : "#ffffff",
										correctLevel : QRCode.CorrectLevel.H
									});
									//qrcode.makeCode(elText.value);
									


				            });
				     }




		});
	}

			$('#content-wrapper').on('click','.generateqr',function(){
	 				var getID = event.target.id;
	 				var index = getID.lastIndexOf("_");
					var tableID = getID.substr(index+1);

					//generate random string
					var randHash = genHash();

							$.ajax({

									type:"POST",
									url:"assets/php/hash.handler.php?funct=updateTable&hash=" + randHash + "&tableid=" + tableID,
									dataType:"json",
									success: console.log('Data Posted')
							});
					//console.log(randHash);
					$('#content-wrapper').empty();
					loadTables();

			});

			function genHash()
			{
			    var text = "";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			    for( var i=0; i < 20; i++ )
			        text += possible.charAt(Math.floor(Math.random() * possible.length));
			    return text;
			}



