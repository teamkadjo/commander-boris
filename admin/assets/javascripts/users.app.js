function loadTable(){
$('#users tbody').html("");
		    $.ajax({
				        type: "POST",
				        url: "assets/php/users.handler.php?funct=viewprod",
				        dataType: 'json',
				        success: function (data) {
						var tbl = $('#users').DataTable();
						$('#users_wrapper .table-caption').text('Inventory Table');
						$('#users_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
						tbl.clear();
				            $.each(data, function (index, value) {
				                var row = $("<tr>" +
												"<td>" + value.id + "</td>" +
												"<td>" + value.username + "</td>" +
												"<td>######################</td>" +
												"<td>" + value.role + "</td>" +
												"<td>" +
													"<div class='btn-group btn-group-justified btn-group-lg'>" +
														"<div style='display:block;margin-bottom:1em;'>" +
															"<a class='btn btn-danger' style='width:100%' id='delbtn'>" +
																"<i class='glyphicon glyphicon-trash'></i>&nbsp;&nbsp;Delete" +
															"</a>" +
														"</div>" +
														"<div style='display:block'>" +
															"<a class='btn btn-info' style='width:100%' id='editbtn'>" +
																"<i class='glyphicon glyphicon-pencil'></i>&nbsp;&nbsp;Edit" +
															"</a>" +
														"</div>" +
													"</div>" +
												"</td>" +
												"<td style='display:none' id='pss'>" + value.password + "</td>" +
												
											"</tr>");
				                tbl.row.add(row).draw(true);


				            });
				        }
			});
	}

		$("#submitnewprod").on("click",function(){
		$('#add_item').modal('hide');
		$('#loading').modal('show');
		var username = $("#username").val();
		var password = $("#password").val();
		var role = $("#role").val();

		$.ajax({

			type: "POST",
			url: "assets/php/users.handler.php?funct=insertprod&username="+username+"&password="+password+"&role="+role,
			dataType: 'json',
			success: console.log('Data Posted')

		});
		
	loadTable();
	$('#loading').modal('hide');
	});
				$('#users tbody').on('click', '#delbtn', function (e) {
			var tr = $(this).closest('tr');
			var productid = $(tr).find("td:eq(0)").text();
			console.log(productid);
			swal({
			  title: 'Are you sure?',
			  text: "You won't be able to revert this!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
			}).then(function () {
					$.ajax({

							type:"POST",
							url:"assets/php/users.handler.php?funct=deleteprod&invid=" + productid,
							dataType:"json",
							success: console.log('Data Posted')
					});
					loadTable();
					return false;	
			  swal(
			    'Deleted!',
			    'Your file has been deleted.',
			    'success'
			  )
			});
			
	});

			$('#users tbody').on('click', '#editbtn', function (e) {
			var tr = $(this).closest('tr');

			var invid 	= $(tr).find("td:eq(0)").text();
			var username = $(tr).find("td:eq(1)").text();
			var role = $(tr).find("td:eq(3)").text();
			var password = $(tr).find("td:eq(5)").text();
			//var password = $('#pass').value();


			$("#edinv_id").val(invid);
			$("#edusername").val(username);
			$("#edpassword").val(password);
			$("#edrole").val(role);

			//console.log($("#edit_id").val());

			$('#edit_item').modal('show');
		return false;
	});
		
		$("#submiteditprod").on('click',function(){
		var invid = $("#edinv_id").val();
		var username = $("#edusername").val();
		var password = $("#edpassword").val();
		var role = $("#edrole").val();


		$.ajax({

				type:"POST",
				url:"assets/php/users.handler.php?funct=editprod&invid="+invid+"&username="+username+"&password="+password+"&role="+role,
				dataType:"json",
				success: console.log('Data Posted')
		});
		
		$("#edit_uploadimage").submit();
		
		$("#edit_item").modal('hide');

		loadTable();

	});









