<?php
ini_set("display_errors",1);
error_reporting(E_ALL);
session_start();
require "database.class.php";

class salesClass {
	private $db;

	function __construct()
	{

		$this->db = new DBConnection("orderingSystem");

	}
	
	
	function convertNumberToWord($num = false)
	{
		$num = str_replace(array(',', ' '), '' , trim($num));
		if(! $num) {
			return false;
		}
		$num = (int) $num;
		$words = array();
		$list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
			'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
		);
		$list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
		$list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
			'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
			'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
		);
		$num_length = strlen($num);
		$levels = (int) (($num_length + 2) / 3);
		$max_length = $levels * 3;
		$num = substr('00' . $num, -$max_length);
		$num_levels = str_split($num, 3);
		for ($i = 0; $i < count($num_levels); $i++) {
			$levels--;
			$hundreds = (int) ($num_levels[$i] / 100);
			$hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ( $hundreds == 1 ? '' : 's' ) . ' ' : '');
			$tens = (int) ($num_levels[$i] % 100);
			$singles = '';
			if ( $tens < 20 ) {
				$tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
			} else {
				$tens = (int)($tens / 10);
				$tens = ' ' . $list2[$tens] . ' ';
				$singles = (int) ($num_levels[$i] % 10);
				$singles = ' ' . $list1[$singles] . ' ';
			}
			$words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
		} //end for loop
		$commas = count($words);
		if ($commas > 1) {
			$commas = $commas - 1;
		}
		return ucwords(implode(' ', $words));
	}	
	

	function getTableList(){

		//$result = $this->db->query("select * from orders ");
		
		//foreach ($result as $value) {
			//if (is_null($value['real_tableid'])) {
				//$result2 = $this->db->query("select * from tables where hash = '".$value['tableId']."'");
				//$this->db->query("UPDATE orders set real_tableid = '". $result2[0]['id']."' where id = '".$value['id']."'");
			//}
			
		//}


		
		$result = $this->db->query("select o.id,t.tablename,os.status_name,o.order_time as time from `orders` o left join 
	orderStatus os on os.id = o.status_id left join `tables` t on t.id = o.tableId where o.status_id < 3");




	
		foreach ($result as $value)
		{
			
			//date_default_timezone_set('Asia/Manila');				
			$date = $value['time'];		
			$the_date = date("H:i:s",strtotime($date));		
			

					print $query = <<<EOD
									<tr>
										<td>{$the_date}</td>
										<td align="center">{$value['tablename']}</td>
										<td align="center">{$value['id']}</td>
										<td>{$value['status_name']}</td>
									</tr>
EOD;
		}

	}
	
	function getSaleslist($data){
		
		
	
	$thecounter = $this->db->query("select count(*) as thecounter from `orders` where status_id = 3 or status_id = 4 ");
	

	$columns = ["o.order_time","t.tablename","o.id","os.status_name"];
	
	$orderby = " order by ".$columns[$data['order'][0]['column']];
	$orderby .= " ".$data['order'][0]['dir'];
	
		
	$result = $this->db->query("select p.dateposted as time,o.id,t.tablename,os.status_name,o.id as clickid,o.grandTotal from `orders` o left join 
	orderStatus os on os.id = o.status_id left join `tables` t on t.id = o.tableid left join payment p on p.order_id = o.id where o.status_id = 3 or o.status_id = 4 ".$orderby." limit ".$data['length']." offset ".$data['start']);

	$the_data = array("draw"=>$data['draw'],"recordsFiltered"=>$thecounter[0]['thecounter'],"recordsTotal"=>$thecounter[0]['thecounter'],"data"=>$result);

	print json_encode($the_data);
}
	


	function getOrderDetails($order_id)
	{
	
		
		$result1= $this->db->query("select * from `orders` where id = '".$order_id."'");
		$the_hash = $result1[0]['tableId'];
		
		//print "select od.id,od.description,od.quantity,p.name,p.price,p.price * od.quantity as total
	 //from orderDetails od left join products p
	//on od.productid = p.id where od.tableHash= '".$the_hash."'";
		
		
		$result = $this->db->query("select od.id,od.tableHash,od.description,od.quantity,p.name,p.price,p.price * od.quantity as total,os.status_name from orderDetails od left join products p on od.productid = p.id left join orderstatus os on os.id = od.status where od.orderid = '".$order_id."'");


		$price = 0;
		$quantity = 0;
		$total = 0;

		foreach ($result as $value)
		{
			
				if (empty($table_hash)) {

				}
				$total += $value['total'];
				$formatted_price = number_format($value['price'],2);
				$formatted_total = number_format($value['total'],2);
					print $query = <<<EOD
									<tr class="editable">
										<td>{$value['name']}</td>
										<td align="right">{$formatted_price}</td>
										<td>Serve</td>
										<td align="center">{$value['quantity']}</td>
										<td align="right">{$formatted_total}</td>
										<td align="right">{$value['status_name']}</td>
									</tr>

EOD;
		}
		
		$taxed_total = $total * .12;
		$grand_total = $total;
		$formatted_taxedtotal = number_format($taxed_total,2);
		$formatted_grandtotal = number_format($grand_total,2);
		print <<<EOD
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td align="center">Tax</td>
						<td align="right">{$taxed_total}</td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td align="center">TOTAL</td>
						<td align="right" id="total_amount_2pay">{$formatted_grandtotal}</td>
						<td></td>
					</tr>
EOD;

		
		
	}

	function getTables()
	{
		$result = $this->db->query("select id,tablename from `tables` order by `id`");
		print json_encode($result);
	}
	
	function updateOrder($data){		
			
			$sql= "UPDATE `orders` SET  tableid = :tableid,status_id = :statusid where id = :orderid";
			$result = $this->db->query($sql,array(":statusid"=>$data['statusid'],":orderid"=>$data['orderid'],":tableid"=>$data['tableid']));

			$sql= "UPDATE `ordersdetails` SET  status = :statusid where orderid = :orderid";
			$result2 = $this->db->query($sql,array(":statusid"=>$data['statusid'],":orderid"=>$data['orderid']));
			echo json_encode(array('result'=>$result)); 
		
		}
	
	function addPayment($data){
	
		$sql = "INSERT into payment(total_amount,cash,datemodified,dateposted,order_id,paytype,cardnumber) values (:total_amount,:cash,NOW(),NOW(),:order_id,:paytype,:cardnumber)";
		$result= $this->db->query($sql,array(":total_amount"=>$data['total_amount'],":cash"=>$data['cash'],":order_id"=>$data['order_id'],":paytype"=>$data['paytype'],":cardnumber"=>$data['creditcard']));
		echo json_encode(array('result'=>$result));
		
		$sql= "UPDATE `orders` SET status_id = :statusid where id = :orderid";
		$result = $this->db->query($sql,array(":statusid"=>3,":orderid"=>$data['order_id']));
		echo json_encode(array('result'=>$result)); 
	
	}
	

	function isLogged_in(){

		if($_SESSION['user_session']) {
			$result = $this->db->query("select *  from users where id = ".$_SESSION['user_session']);
			echo json_encode(array('is_logged'=>$_SESSION['user_session'],'userinfo'=>$result[0])); 
		}
		else {
			echo json_encode(array('is_logged'=>0,'userinfo'=>array())); 
		}
	
	}

	function get_UserInfo($data){
		$result = $this->db->query("select *  from users where id = ".$data['id']);
		print json_encode($result);	
	}

	function get_OrderInfo($data){
		$result = $this->db->query("SELECT *  from `orders` where id = ".$data['id']);	
		$result2 = $this->db->query("SELECT od.id,od.tableHash,od.description,od.quantity,p.name,p.price,p.price * od.quantity as total,os.status_name from orderDetails od left join products p on od.productid = p.id left join orderstatus os on os.id = od.status where od.orderid = ".$data['id']);
		$result3 = $this->db->query("SELECT cash - total_amount as changed,cash,paytype,cardnumber from payment where order_id = '".$data['id']."'");
		
		$result[0]['details'] = $result2;	
		$result[0]['change'] = $result3[0]['changed'];	
		$result[0]['cash'] = $result3[0]['cash'];	
		$result[0]['paytype'] = $result3[0]['paytype'];	
		$result[0]['cardnumber'] = "XXXXXXXXXXXX".substr($result3[0]['cardnumber'], -4);	
		$result[0]['totalName'] = $this->convertNumberToWord($result[0]['grandTotal']);
		$result[0]['totalNumberf'] = number_format($result[0]['grandTotal'],2);
		$d = new DateTime($result[0]['order_time']);
		$result[0]['dateNow'] = $d->format("m-d-Y");

		print json_encode($result);	
	}
	

}

$_action = $_REQUEST['action'];

$salesTable = new salesClass();

switch($_action) {
	case "get-table-list": $salesTable->getTableList();
		break;
	case "get-order-details":$salesTable->getOrderDetails($_REQUEST['order_id']);
		break;
	case "get-tables":$salesTable->getTables();
		break;
	case "update-Order":$salesTable->updateOrder($_REQUEST);
		break;
	case "add-Payment":$salesTable->addPayment($_REQUEST);
		break;
	case "get-Sales":$salesTable->getSaleslist($_REQUEST);
		break;
	case "is_logged": $salesTable->isLogged_in();
		break;
	case "get_userinfo": $salesTable->get_UserInfo($_REQUEST);
		break;
	case "get_orderinfo": $salesTable->get_OrderInfo($_REQUEST);
		break;

}



?>
