<?php
/*
Inventory Handler
Handles all functions for the inventory interface in the admin menu
*/


include('database.class.php');
$connect = new DBConnection('orderingSystem');

$func = $_REQUEST['funct'];

switch($func){
	case 'viewprod':
		ViewProducts();
		break;
	case 'insertprod':
		
		$stocknum = $_REQUEST['stocknum'];
		$productcode = $_REQUEST['productcode'];
		$description = $_REQUEST['description'];
		$quantity = $_REQUEST['quantity'];
		$supplier = $_REQUEST['supplier'];

		InsertProducts($stocknum,$productcode,$description,$quantity,$supplier);
		break;
	case 'editprod':

		$stocknum = $_REQUEST['stocknum'];
		$productcode = $_REQUEST['productcode'];
		$description = $_REQUEST['description'];
		$quantity = $_REQUEST['quantity'];
		$supplier = $_REQUEST['supplier'];
		$invid = $_REQUEST['invid'];
		EditProducts($stocknum,$productcode,$description,$quantity,$supplier,$invid);
		break;
	case 'deleteprod':
		$prodid = $_REQUEST['invid'];
		DeleteProducts($prodid);
		break;	

}	


function ViewProducts(){
	global $connect;
		$products = $connect->Select_Query("Select * from inventory order by id desc");
	echo($products);
}

function InsertProducts($stocknum,$productcode,$description,$quantity,$supplier){
	global $connect;
		$returnedmsg = $connect->Exec_Query("Insert into inventory(stocknum,productcode,description,quantity,supplier) values ('$stocknum','$productcode','$description','$quantity','$supplier')");
	echo($returnedmsg);
}

function EditProducts($stocknum,$productcode,$description,$quantity,$supplier,$invid){
	global $connect;
		$returnedmsg = $connect->Exec_Query("Update inventory set stocknum='$stocknum',productcode='$productcode',description='$description',quantity='$quantity',supplier='$supplier' where id = $invid");
	echo($returnedmsg);
}

function DeleteProducts($prodid){
	global $connect;
		$returnedmsg = $connect->Exec_Query("Delete from inventory where id=$prodid");
	echo($returnedmsg);
}
?>