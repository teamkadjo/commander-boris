<?php
/*
Order Handler
Handles all functions for the order interface in the admin menu
*/

include('database.class.php');

$connect = new DBConnection('orderingSystem');

$func = $_REQUEST['funct'];

switch($func){

	case 'orders':
		$tableid = $_REQUEST['tableid'];
		getOrders($tableid);
		break;
	case 'tables':	
		getTables();
		break;
	case 'orderdetails':
		$orderid = $_REQUEST['orderid'];
		getOrderDetails($orderid);
		break;
}



function getOrders($tableid){
	global $connect;
	$orders = $connect->Select_Query("Select a.id, b.status_name, a.order_time, a.tableid from orders a INNER JOIN orderStatus b ON a.status_id = b.id where a.tableid = '" . $tableid . "'");
	echo($orders);
}

function getTables(){
	global $connect;
	$tables = $connect->Select_Query("Select * from tables");
	echo($tables);
}


function getOrderDetails($orderid){
	global $connect;
	$orderdetails = $connect->Select_Query("Select b.name, a.quantity, c.status_name from orderdetails a INNER JOIN products b on a.productid = b.id  INNER JOIN orderstatus c on a.status = c. id where orderid ='$orderid'");
	echo($orderdetails);
}


?>