<?php
/*
Product Handler
Handles all functions for the product interface in the admin menu
*/


include('database.class.php');
$connect = new DBConnection('orderingSystem');


$func = $_REQUEST['funct'];

switch($func){
	case 'viewprod':
		ViewProducts();
		break;
	case 'insertprod':
		$imglink = $_REQUEST['imglink'];
		$name = $_REQUEST['name'];
		$desc = $_REQUEST['desc'];
		$mealcourse = $_REQUEST['mealcourse'];
		$servings = $_REQUEST['servings'];
		$price = $_REQUEST['price'];
		$quantity = $_REQUEST['quantity'];
		$servingtime = $_REQUEST['servingtime'];
		$expdate = $_REQUEST['expdate'];
		InsertProducts($imglink,$name,$desc,$price,$mealcourse,$servings,$quantity,$servingtime,$expdate);
		break;
	case 'editprod':
		$imglink = $_REQUEST['imglink'];
		$name = $_REQUEST['name'];
		$desc = $_REQUEST['desc'];
		$mealcourse = $_REQUEST['mealcourse'];
		$servings = $_REQUEST['servings'];
		$price = $_REQUEST['price'];
		$prodid = $_REQUEST['prodid'];
		$quantity = $_REQUEST['quantity'];
		$servingtime = $_REQUEST['servingtime'];
		$expdate = $_REQUEST['expdate'];

		EditProducts($imglink,$name,$desc,$price,$mealcourse,$servings,$prodid,$quantity,$servingtime,$expdate);
		break;
	case 'deleteprod':
		$prodid = $_REQUEST['prodid'];
		DeleteProducts($prodid);
		break;	
	case 'uploadimage':	
		$image = $_FILES["add-input-file-preview"]["name"];	
		$tmp_name = $_FILES['add-input-file-preview']['tmp_name'];
		UploadImage($image,$tmp_name);
		break;
	case 'edituploadimage':
		$image = $_FILES["edit-input-file-preview"]["name"];	
		$tmp_name = $_FILES['edit-input-file-preview']['tmp_name'];
		UploadImage($image,$tmp_name);
		break;	
}	


function ViewProducts(){
	global $connect;
		$products = $connect->Select_Query("Select * from products  where status='active' order by id desc");
	echo($products);
}

function InsertProducts($imglink,$name,$desc,$price,$mealcourse,$servings,$quantity,$servingtime,$expdate){
	global $connect;
		$returnedmsg = $connect->Exec_Query("Insert into products(name,description,price,mealCourse,servings,productImage,quantity,status,servingtime,expdate) values ('$name','$desc','$price','$mealcourse',$servings,'$imglink',$quantity,'active','$servingtime','$expdate')");
	echo($returnedmsg);
}

function EditProducts($imglink,$name,$desc,$price,$mealcourse,$servings,$prodid,$quantity,$servingtime,$expdate){
	global $connect;
		$returnedmsg = $connect->Exec_Query("Update products set name='$name',description='$desc',price='$price',mealCourse='$mealcourse',servings='$servings',productImage='$imglink',quantity='$quantity',status='active',servingtime='$servingtime',expdate='$expdate' where id = $prodid");
	echo($returnedmsg);
}

function DeleteProducts($prodid){
	global $connect;
		$returnedmsg = $connect->Exec_Query("Update products set status='inactive' where id=$prodid");
	echo($returnedmsg);
}
function UploadImage($image,$tmp_name){
	if (file_exists("../images/products/" . $image)) {
		echo $_FILES["file"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
	}else{
		$sourcePath = $tmp_name; 
		$targetPath = "../images/products/".$image; 
		move_uploaded_file($sourcePath,$targetPath) ; 
		echo 'Image Upload Succesfull!';
	}	
}
?>













