<?php

class DBConnection{
	public $db;
		/*
		Connects to the Specified Database when Class is initialized
		*/
	   function __construct($dbname) {
	      
	      try{
			$host = 'localhost';
			$db   = $dbname;
			$user = 'rebeladmin';
			$pass = 'r3b3l4dm1n!@#';
			$charset = 'utf8';

			$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
			$opt = [
			    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			    PDO::ATTR_EMULATE_PREPARES   => false,
			];

			$PDOinstance = new PDO($dsn, $user, $pass, $opt);
			$this->db = $PDOinstance;
			}catch(PDOException $ex){

				echo "There was a problem with your connection! <br> Error Message is: (". $ex->getMessage() . ") ";

			}	
	   }
	   /*
		Returns a JSON Encoded Array of Results
	   */
	   function Select_Query($querystring){
	   		if($this->db != null){
		   		$conn = $this->db;
		   		$stmt = $conn->query($querystring);
		   		$rows = $stmt->fetchAll();
		   		return json_encode($rows);
	   		}
		}

   	   function Exec_Query($querystring){
	   	   	if($this->db != null){
	   	   		$conn = $this->db;
		   	   	$affected_rows = $conn->exec($querystring);
				echo $affected_rows.' were affected';
			}
		}
		
		function query($sql,$params=[]){
				if ($params != []) {
					$conn = $this->db;
					$query= $conn->prepare($sql);
					$result = $query->execute($params);
					return $result;
				}
				else {
					$conn = $this->db;
					$stmt = $conn->query($sql);
					$rows = $stmt->fetchAll();
					return $rows;
				}
   	   }




}


?>
