function loadtableTables(){
		$('#tables tbody').html("");
		$.ajax({

			type: "POST",
			url: "assets/php/orders.handler.php?funct=tables",
			dataType: 'json',
			success: function (data) {
									var tbl = $('#tables').DataTable();
									$('#tables_wrapper .table-caption').text('Tables');
									$('#tables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
									tbl.clear();
				            $.each(data, function (index, value) {

											var row = $("<tr>" +
												"<td>" + value.id + "</td>" +
												"<td>" + value.tablename + "</td>" +
											"</tr>");
											tbl.row.add(row).draw(true);



				            });
				     }




		});
}

$('#tables tbody').on('click','tr', function (e) {
	tableid = $(this).find("td:eq(0)").text();
	//call show order function
	showOrders(tableid);
});

$('#orders tbody').on('click','tr', function (e) {
	orderid = $(this).find("td:eq(0)").text();
	//call show order function
	showOrderDetails(orderid);
});

function showOrders(tableid){
	$('#orders tbody').html("");
	var tbl = $('#orders').DataTable();
	tbl.clear();
	$('#tables_wrapper .table-caption').text('Tables');
	$('#tables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
	
		$.ajax({
			type: "POST",
			url: "assets/php/orders.handler.php?funct=orders&tableid="+ tableid,
			dataType: 'json',
			success: function (data) {
							
							
							$.each(data, function (index, value) {

								var row = $("<tr>" +
												"<td>" + value.id + "</td>" +
												"<td>" + value.status_name + "</td>" +
												"<td>" + value.order_time + "</td>" +
											"</tr>");
											tbl.row.add(row).draw(true);
							});

			}

		});

}

function showOrderDetails(orderid){
	$('#orderdetails tbody').html("");
	var tbl = $('#orderdetails').DataTable();
	tbl.clear();
	$('#tables_wrapper .table-caption').text('Tables');
	$('#tables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

		$.ajax({
			type: "POST",
			url: "assets/php/orders.handler.php?funct=orderdetails&orderid=" + orderid,
			dataType: 'json',
			success: function (data) {
							$.each(data, function (index, value) {

								var row = $("<tr>" +
												"<td>" + value.name + "</td>" +
												"<td>" + value.quantity + "</td>" +
												"<td>" + value.status_name + "</td>" +
											"</tr>");
											tbl.row.add(row).draw(true);
							});
				
			}

		});

}