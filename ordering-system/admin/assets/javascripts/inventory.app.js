function loadTable(){
$('#inventory tbody').html("");
		    $.ajax({
				        type: "POST",
				        url: "assets/php/inventory.handler.php?funct=viewprod",
				        dataType: 'json',
				        success: function (data) {
						var tbl = $('#inventory').DataTable();
						$('#inventory_wrapper .table-caption').text('Inventory Table');
						$('#inventory_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
						tbl.clear();
				            $.each(data, function (index, value) {
				                var row = $("<tr>" +
												"<td>" + value.id + "</td>" +
												"<td>" + value.stocknum + "</td>" +
												"<td>" + value.productcode + "</td>" +
												"<td>" + value.description + "</td>" +
												"<td>" + value.quantity + "</td>" +
												"<td>" + value.supplier + "</td>" +
												"<td>" +
													"<div class='btn-group btn-group-justified btn-group-lg'>" +
														"<div style='display:block;margin-bottom:1em;'>" +
															"<a class='btn btn-danger' style='width:100%' id='delbtn'>" +
																"<i class='glyphicon glyphicon-trash'></i>&nbsp;&nbsp;Delete" +
															"</a>" +
														"</div>" +
														"<div style='display:block'>" +
															"<a class='btn btn-info' style='width:100%' id='editbtn'>" +
																"<i class='glyphicon glyphicon-pencil'></i>&nbsp;&nbsp;Edit" +
															"</a>" +
														"</div>" +
													"</div>" +
												"</td>" +
												
											"</tr>");
				                tbl.row.add(row).draw(true);


				            });
				        }
			});
	}

	$("#submitnewprod").on("click",function(){
		$('#add_item').modal('hide');
		$('#loading').modal('show');
		var stocknum = $("#inv_stocknum").val();
		var productcode = $("#inv_prodcode").val();
		var description = $("#inv_desc").val();
		var quantity = $("#inv_quantity").val();
		var supplier = $("#inv_supplier").val();
		$.ajax({

			type: "POST",
			url: "assets/php/inventory.handler.php?funct=insertprod&stocknum="+stocknum+"&productcode="+productcode+"&description="+description+"&quantity="+quantity+"&supplier="+supplier,
			dataType: 'json',
			success: console.log('Data Posted')

		});
		
	loadTable();
	$('#loading').modal('hide');
	});

		$('#inventory tbody').on('click', '#delbtn', function (e) {
			var tr = $(this).closest('tr');
			var productid = $(tr).find("td:eq(0)").text();
			console.log(productid);
			swal({
			  title: 'Are you sure?',
			  text: "You won't be able to revert this!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
			}).then(function () {
					$.ajax({

							type:"POST",
							url:"assets/php/inventory.handler.php?funct=deleteprod&invid=" + productid,
							dataType:"json",
							success: console.log('Data Posted')
					});
					loadTable();
					return false;	
			  swal(
			    'Deleted!',
			    'Your file has been deleted.',
			    'success'
			  )
			});
			
	});


			$('#inventory tbody').on('click', '#editbtn', function (e) {
			var tr = $(this).closest('tr');
			var invid 	= $(tr).find("td:eq(0)").text();
			var stocknum = $(tr).find("td:eq(1)").text();
			var productcode = $(tr).find("td:eq(2)").text();
			var description = $(tr).find("td:eq(3)").text();
			var quantity = $(tr).find("td:eq(4)").text();
			var supplier = $(tr).find("td:eq(5)").text();


			$("#edinv_id").val(invid);
			$("#edinv_stocknum").val(stocknum);
			$("#edinv_prodcode").val(productcode);
			$("#edinv_desc").val(description);
			$("#edinv_quantity").val(quantity);
			$("#edinv_supplier").val(supplier);
			//console.log($("#edit_id").val());

			$('#edit_item').modal('show');
		return false;
	});

	$("#submiteditprod").on('click',function(){
		var invid = $("#edinv_id").val();
		var stocknum = $("#edinv_stocknum").val();
		var productcode = $("#edinv_prodcode").val();
		var description = $("#edinv_desc").val();
		var quantity = $("#edinv_quantity").val();
		var supplier = $("#edinv_supplier").val();

		$.ajax({

				type:"POST",
				url:"assets/php/inventory.handler.php?funct=editprod&invid="+invid+"&stocknum="+stocknum+"&productcode="+productcode+"&description="+description+"&quantity="+quantity+"&supplier="+supplier,
				dataType:"json",
				success: console.log('Data Posted')
		});
		
		$("#edit_uploadimage").submit();
		
		$("#edit_item").modal('hide');

		loadTable();

	});











































