function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


var hash = getUrlParameter('hash');

var tableid = getUrlParameter('tableid');


$("#tableid").text(tableid);
$("#orderlink").text(hash);

var qrcode = new QRCode(document.getElementById("qrcode"), {
    text: "http://restorante.rebel-it.tech/?table=" + hash,
    width: 200,
    height: 200,
    colorDark : "#000000",
    colorLight : "#ffffff",
    correctLevel : QRCode.CorrectLevel.H
});