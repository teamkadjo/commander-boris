function loadTable(){

		    $.ajax({
				        type: "POST",
				        url: "assets/php/products.handler.php?funct=viewprod",
				        dataType: 'json',
				        success: function (data) {
						var tbl = $('#products').DataTable();
						$('#products_wrapper .table-caption').text('Products Table');
						$('#products_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
						tbl.clear();
				            $.each(data, function (index, value) {
				                var row = $("<tr>" +
												"<td style='display:none'>" + value.id + "</td>" +
												"<td align='center'><img src='" + value.productImage + "' class='img-thumbnail' width='100' height='100'></td>" +
												"<td>" + value.name + "</td>" +
												"<td>" + value.description + "</td>" +
												"<td>" + value.mealCourse + "</td>" +
												"<td>" + value.servings + "</td>" +
												"<td>" + value.price + "</td>" +
												"<td>" + value.quantity + "</td>" +
												"<td>" + value.servingtime + "</td>" +
												"<td>" + value.expdate + "</td>" +
												"<td>" +
													"<div class='btn-group btn-group-justified btn-group-lg'>" +
														"<div style='display:block;margin-bottom:1em;'>" +
															"<a class='btn btn-danger' style='width:100%' id='delbtn'>" +
																"<i class='glyphicon glyphicon-trash'></i>&nbsp;&nbsp;Delete" +
															"</a>" +
														"</div>" +
														"<div style='display:block'>" +
															"<a class='btn btn-info' style='width:100%' id='editbtn'>" +
																"<i class='glyphicon glyphicon-pencil'></i>&nbsp;&nbsp;Edit" +
															"</a>" +
														"</div>" +
													"</div>" +
												"</td>" +
												"<td style='display:none'>" + value.productImage + "</td>" +
											"</tr>");
				                tbl.row.add(row).draw(true);


				            });
				        }
			});
	}
	init.push(function () {

		loadTable();
		
	})
	window.PixelAdmin.start(init);

	//submit New Product
	$("#submitnewprod").on("click",function(){
		$('#add_item').modal('hide');
		$('#loading').modal('show');
		var prodimglink1 = $("#add_itemimglink").val();
		var prodimglink =JSON.stringify(prodimglink1).slice(1, -1);
		var prodname = $("#add_itemname").val();
		var proddesc = $("#add_itemdescription").val();
		var prodmealcourse = $("#add_itemmealcourse").val();
		var proditemserving = $("#add_itemserving").val();
		var price = $("#add_itemprice").val();
		var quantity = $("#add_itemquantity").val();
		var servingtime = $("#add_servingtime").val();
		var expdate = $("#add_expdate").val();
		$.ajax({

			type: "POST",
			url: "assets/php/products.handler.php?funct=insertprod&imglink="+prodimglink+"&name="+prodname+"&desc="+proddesc+"&price="+price+"&mealcourse="+prodmealcourse+"&servings="+proditemserving+"&quantity="+quantity+"&servingtime="+servingtime+"&expdate="+expdate,
			dataType: 'json',
			success: console.log('Data Posted')

		});
		$("#uploadimage").submit();
		location.reload();

	});


	$('#products tbody').on('click', '#delbtn', function (e) {
			var tr = $(this).closest('tr');
			var productid = $(tr).find("td:eq(0)").text();
			console.log(productid);
			swal({
			  title: 'Are you sure?',
			  text: "You won't be able to revert this!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
			}).then(function () {
					$.ajax({

							type:"POST",
							url:"assets/php/products.handler.php?funct=deleteprod&prodid=" + productid,
							dataType:"json",
							success: console.log('Data Posted')
					});
					location.reload();	
					return false;	
			  swal(
			    'Deleted!',
			    'Your file has been deleted.',
			    'success'
			  )
			});
			
	});

	//Display Edit Product
	$('#products tbody').on('click', '#editbtn', function (e) {
			var tr = $(this).closest('tr');

			var productid = $(tr).find("td:eq(0)").text();
			var prodimglink = $(tr).find("td:eq(9)").text();
			
			var prodname = $(tr).find("td:eq(2)").text();
			var proddesc = $(tr).find("td:eq(3)").text();
			var prodmealcourse = $(tr).find("td:eq(4)").text();
			var proditemserving = $(tr).find("td:eq(5)").text();
			var price = $(tr).find("td:eq(6)").text();
			var quantity = $(tr).find("td:eq(7)").text();
			var servingtime = $(tr).find("td:eq(8)").text();
			var expdate = $(tr).find("td:eq(9)").text();

			$("#edit_id").val(productid);
			$("#edit_itemimglink").val(prodimglink);
			$("#edit_itemname").val(prodname);
			$("#edit_itemdescription").val(proddesc);
			$("#edit_itemmealcourse").val(prodmealcourse);
			$("#edit_itemserving").val(proditemserving);
			$("#edit_itemprice").val(price);
			$("#edit_itemquantity").val(quantity);
			$("#edit_servingtime").val(servingtime);
			$("#edit_expdate").val(expdate);
			//console.log($("#edit_id").val());

			$('#edit_item').modal('show');
		return false;
	});

	//Submit Edit Product
	$("#submiteditprod").on('click',function(){

		var productid = $("#edit_id").val();
		var prodname = $("#edit_itemname").val();
		var proddesc = $("#edit_itemdescription").val();
		var prodmealcourse = $("#edit_itemmealcourse").val();
		var proditemserving = $("#edit_itemserving").val();
		var price = $("#edit_itemprice").val();
		var prodimglink1 = $("#edit_itemimglink").val();
		var prodimglink =JSON.stringify(prodimglink1).slice(1, -1);
		var quantity = $("#edit_itemquantity").val();
		var servingtime = $("#edit_servingtime").val();
		var expdate = $("#edit_expdate").val();
		$.ajax({

				type:"POST",
				url:"assets/php/products.handler.php?funct=editprod&imglink="+prodimglink+"&name="+prodname+"&desc="+proddesc+"&price="+price+"&mealcourse="+prodmealcourse+"&servings="+proditemserving+"&prodid="+productid+"&quantity="+quantity+"&servingtime="+servingtime+"&expdate="+expdate,
				dataType:"json",
				success: console.log('Data Posted')
		});
		
		$("#edit_uploadimage").submit();
		
		$("#edit_item").modal('hide');

		location.reload();

	});

	//Add Item File Chooser Event

	$("#add-input-file-preview").on('change',function(){
		var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
		$("#add_itemimglink").val('assets\\images\\products\\' + filename);
	});

	// Edit Item File Chooser Event
	$("#edit-input-file-preview").on('change',function(){
		var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
		$("#edit_itemimglink").val('assets\\images\\products\\' + filename);
	});

//JS for add product Image Button
$(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
        }, 
         function () {
           $('.image-preview').popover('hide');
        }
    );    
});


$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    });  
});

$("#uploadimage").on('submit',function(e){
	e.preventDefault();
		//Upload Image to Server
		$.ajax({
			type: "POST",
			url: "assets/php/products.handler.php?funct=uploadimage",
			contentType: false,      
			cache: false,        
			processData:false,  
			data: new FormData(this),
			success:function(data){
				console.log(data);
			}
		});

		$('#loading').modal('hide');
});

$("#edit_uploadimage").on('submit',function(e){
	e.preventDefault();
		//Upload Image to Server
		$.ajax({
			type: "POST",
			url: "assets/php/products.handler.php?funct=edituploadimage",
			contentType: false,      
			cache: false,        
			processData:false,  
			data: new FormData(this),
			success:function(data){
				console.log(data);
			}
		});

		$('#loading').modal('hide');
});










