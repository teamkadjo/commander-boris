	var dtable = null;
	
	

	function getUrlVars()
	{
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for(var i = 0; i < hashes.length; i++)
		{
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	}

	function format2(n, currency) {
		return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	}
	

	function commaSeparateNumber(val) {
		while (/(\d+)(\d{3})/.test(val.toString())) {
			val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
		}
		return val;
	}	
	
jQuery(document).ready(function(){

		
				jQuery.ajax({url:'assets/php/sales.handler.php',data:{action:'get_orderinfo',id: getUrlVars()['id']},dataType:'json',success:function(data){
					/* jQuery("#amount").text(data[0].totalName);
					jQuery("#amount_number").text(data[0].totalNumberf); */
					jQuery("#dateNow").text(data[0].dateNow);
					jQuery("#orNumber").text(data[0].id); 
					
					jQuery("#descript_detail").html("<td>TOTAL </td><td style='text-align:right'>" + data[0].totalNumberf + "</td>");
					
					jQuery.each(data[0].details,function(i,DAT){
						
						jQuery("<tr style='background:cornsilk'><td>" + DAT.name+ "</td><td style='text-align:right'>" + format2(DAT.total,"") + "</td></tr>").insertAfter("#descript_detail");
					});
					
					jQuery("<tr><td>Cash</td><td style='text-align:right'>" + format2(data[0].cash,"") + "</td></tr>").insertBefore("#last_line");
					jQuery("<tr><td>Changed</td><td style='text-align:right'>" + format2(data[0].change,"") + "</td></tr>").insertBefore("#last_line");
					
					
				}});
				
							
	});
