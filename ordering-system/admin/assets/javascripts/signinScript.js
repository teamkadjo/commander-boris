var init = [];

// Sign In Validator
$(function () {
    "use strict";

    $("#signin-form_id").validate({
        // Rules for form validation
        rules: {
            signin_username: {
                required: true,
                minlength: 2,
            },
            signin_password: {
                required: true,
                minlength: 2
            }
        },
        // Messages for form validation
        messages: {
            signin_username: {
                required: 'Please enter your username'
            },
            signin_password: {
                required: 'Please enter your password'
            }
        },
        validClass: "validated",

        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());

        },

        submitHandler: submitForm
    });

});
// End of Sign In Validator


// Resize BG
init.push(function () {
    var $ph = $('#page-signin-bg'),
        $img = $ph.find('> img');

    $(window).on('resize', function () {
        $img.attr('style', '');
        if ($img.height() < $ph.height()) {
            $img.css({
                height: '100%',
                width: 'auto'
            });
        }
    });
});

// Show/Hide password reset form on click
init.push(function () {
    $('#forgot-password-link').click(function () {
        $('#password-reset-form').fadeIn(400);
        return false;
    });
    $('#password-reset-form .close').click(function () {
        $('#password-reset-form').fadeOut(400);
        return false;
    });
});

// Setup Sign In form validation
init.push(function () {
    $("#signin-form_id").validate({
        focusInvalid: true, errorPlacement: function () {
        }
    });

});

// Setup Password Reset form validation
init.push(function () {
    $("#password-reset-form_id").validate({
        focusInvalid: true, errorPlacement: function () {
        }
    });

});

window.PixelAdmin.start(init);


// Login
$(".response").hide();

function submitForm() {
    var data = $("#signin-form_id").serialize();

    $.ajax({

        type: 'POST',
        url: 'assets/php/login.php',
        data: data,
        beforeSend: function () {
            $(".response").fadeIn();
           setTimeout('$(".response").fadeOut()',3000);
        },
        success: function (response) {
            if (response.trim() == "ok") {

                $(".response").html('<span>SIGNING IN</span> <img src="assets/images/signin/ellipsis.gif" draggable="false">');
                setTimeout(' window.location.href = "index2.html"; ', 1500);
            }
            else {
                $(".response").html('<i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i> <span class="username_password_error">Username or Password does not exist!</span>');
                return false;
            }
        }
    });
    return false;
}

// End of Login

