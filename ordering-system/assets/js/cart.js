$(function () {

    // domain name
    var domain = window.location.hostname;

    //query string
    params = new URL(document.URL).searchParams;
    var tableNumber = params.get('table');

    // number format commas
    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

    // no cart item yet
    function alertNoCart() {
        $('.single-item').hide();
        swal({
            title: 'No item',
            text: "No item in cart yet!",
            type: 'warning',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            timer: 4000,
            allowOutsideClick: false
        }).then(
            function () {
                swal({
                    title: 'Redirecting...',
                    timer: 2000,
                    type: 'info',
                    showConfirmButton: false
                }).then(
                    function () {
                    },
                    function (dismiss) {
                        if (dismiss === 'timer') {
                            window.location = 'http://' + domain + "?table=" + tableNumber;
                        }
                    }
                )
            }, function (dismiss) {
                if (dismiss) {
                    swal({
                        title: 'Redirecting...',
                        timer: 2000,
                        type: 'info',
                        showConfirmButton: false
                    }).then(
                        function () {
                        },
                        function (dismiss) {
                            if (dismiss === 'timer') {
                                window.location = 'http://' + domain + "?table=" + tableNumber;
                            }
                        }
                    )
                }
            });
    }


    // confirms table hash
    $.ajax({
        type: "POST",
        dataType: 'json',
        data: {
            tableHash: tableNumber
        },
        url: "assets/php/cart.php?action=confirmTableHash",
        success: function (data) {

            var params2 = params.toString().length;

            //confirming table hash if valid
            if ((params == 'table=' || params != 'table=' && params2 <= 0) && (data == 0)) {
                $(".single-item").hide();
                swal({
                    title: 'Whoops!',
                    text: "No content for you here!",
                    type: 'warning',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK',
                    timer: 4000,
                    allowOutsideClick: false
                }).then(
                    function () {
                        swal({
                            title: 'Redirecting...',
                            timer: 2000,
                            type: 'info',
                            showConfirmButton: false
                        }).then(
                            function () {
                            },
                            function (dismiss) {
                                if (dismiss === 'timer') {
                                    window.location = 'http://' + domain;
                                }
                            }
                        )
                    }, function (dismiss) {
                        if (dismiss) {
                            swal({
                                title: 'Redirecting...',
                                timer: 2000,
                                type: 'info',
                                showConfirmButton: false
                            }).then(
                                function () {
                                },
                                function (dismiss) {
                                    if (dismiss === 'timer') {
                                        window.location = 'http://' + domain;
                                    }
                                }
                            )
                        }
                    });
            } else {
//get all items from cart

                function getItems() {

                    var delay = 5000;

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        data: {
                            tableHash: tableNumber
                        },
                        url: "assets/php/cart.php?action=getCart",
                        success: function (data, response) {

                            $.each(data, function (index, value) {
                                var cartProducts = $("<div class='row'>" +
                                    "<div class='col-xs-2'><img class='img-responsive' src='admin/" + value.productImage + "' draggable='false'> </div>" +
                                    "<div class='col-xs-4'> " +
                                    "<h4 class='product-name'><strong>" + value.name + "</strong></h4>" +
                                    "<h4><small>" + value.description + "</small></h4>" +
                                    "<label class='label label-primary'>To be serve in " + value.servingtime + "</label>" +
                                    "</div>" +
                                    "<div class='col-xs-6'>" +
                                    "<div class='col-xs-6 text-right'>" +
                                    "<h6><strong>₱" + value.price + "<span class='text-muted'>x</span></strong></h6>" +
                                    "</div>" +
                                    "<div class='col-xs-4'>" +
                                    "<input type='text' class='form-control input-sm' value='" + value.totalQty + "' disabled>" +
                                    "</div>" +
                                    "<div class='col-xs-2'>" +
                                    "<button id='delete_" + value.cartId + "' type='button' class='btn btn-link btn-xs'>" +
                                    " <span class='glyphicon glyphicon-trash'> </span>" +
                                    "</button>" +
                                    "</div>" +
                                    "</div>" +
                                    "  </div>" +
                                    "<br>");

                                $("#cartProducts").append(cartProducts);

                                //alert(JSON.stringify(data));

                                // delete item on cart
                                $("#delete_" + value.cartId).click(function () {

                                    swal({
                                        title: 'Remove<br>' + value.name + '<br>',
                                        text: 'Are you sure?',
                                        type: 'info',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Yes, remove this order!',
                                        allowOutsideClick: false,

                                    }).then(function () {


                                        $.ajax({
                                            type: "POST",
                                            data: {
                                                tableNum: tableNumber,
                                                productId: value.productId,
                                                totalQty: value.totalQty
                                            },
                                            url: "assets/php/cart.php?action=deleteItem",
                                            success: function (result) {
                                                swal({
                                                    title: 'Removed!',
                                                    text: 'Your item has been removed to cart.',
                                                    type: 'success',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                                $('#loading_img').fadeIn('slow');
                                                $('#cartProducts').html(result);
                                            },
                                        }).done(function () {

                                            //cart count
                                            $.ajax({
                                                type: "POST",
                                                dataType: 'json',
                                                data: {
                                                    tableNum: tableNumber
                                                },
                                                url: "assets/php/addToCart.php?action=cartCount",
                                                success: function (data) {

                                                    if (data.qty == null) {
                                                        var totalQty = data.qty = 0;
                                                    } else {
                                                        var totalQty = data.qty;
                                                    }
                                                    $("#cartCount").empty();
                                                    $("#cartCount").append(totalQty);
                                                }
                                            });


                                            getItems();
                                            grandTotal();
                                            $("#grandTotal").empty();
                                            $('#loading_img').fadeOut('slow');
                                        });

                                    });
                                });

                            });
                        }
                    });
                }

                getItems();

                //grand total
                function grandTotal() {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        data: {
                            tableNum: tableNumber
                        },
                        url: "assets/php/cart.php?action=grandTotal",
                        success: function (data) {
                            $("#grandTotal").append("₱" + numberWithCommas(data));

                            if (data == 0) {
                                $('#placeOrderBtn').attr("disabled", "disabled");
                                $('#cartProducts').html('<h1 align="center">NO ITEM!</h1>');
                            }

                            setTimeout(function () {
                                if (data == 0) {
                                    swal({
                                        title: 'Opps!',
                                        text: 'You have no item in cart Redirecting to homepage...',
                                        type: 'info',
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        allowOutsideClick: false,
                                        timer: 2000
                                    }).then(function () {
                                        },
                                        function (dismiss) {
                                            if (dismiss === 'timer') {
                                                window.location = 'http://' + domain + '/?table=' + tableNumber;
                                            }
                                        }
                                    );
                                }
                            }, 2500);
                        }
                    });
                }

                grandTotal();

            }
        }
    });

    $("#placeOrderBtn").click(function () {

        swal({
            title: 'Place order',
            text: "Are you sure?",
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, place my order!',
            allowOutsideClick: false
        }).then(function () {
            $("#placeOrderBtn").prop('disabled', true);
            swal({
                title: 'Place Order Success!',
                text: 'Your order is being prepare.',
                type: 'success',
                timer: 3000
            }).then(
                function () {

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        data: {
                            tableNum: tableNumber
                        },
                        url: "assets/php/cart.php?action=placeOrder",
                        success: function (data) {
                            window.location = 'order.html?table=' + tableNumber;
                        }
                    });

                },
                function (dismiss) {
                    if (dismiss === 'timer') {

                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            data: {
                                tableNum: tableNumber
                            },
                            url: "assets/php/cart.php?action=placeOrder",
                            success: function (data) {
                                window.location = 'order.html?table=' + tableNumber;
                            }
                        });

                    }
                }
            )
        })
    });

});