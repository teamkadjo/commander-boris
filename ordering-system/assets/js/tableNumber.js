$(function () {

    // domain name
    var domain = window.location.hostname;

    //query string
    params = new URL(document.URL).searchParams;
    var tableNumber = params.get('table');

    // get meal course
    $.ajax({
        type: "POST",
        url: "assets/php/products.php?action=getMealCourse",
        dataType: 'json',
        success: function (data) {

            $.each(data, function (index, value) {
                var mealCouse = value.mealCourse.replace(/\s+/g, '');

                var mealCourse = $("<li>" +
                    "<a data-toggle='tab' role='tab' href='#" + mealCouse + "-tab'> " + value.mealCourse + " </a>" +
                    "</li>");

                var mealCourseTab = $("<div id='" + mealCouse + "-tab' class='tab-pane fade in' role='tabpanel'>" +
                    "<div class='row' id='" + mealCouse + "'></div>" +
                    "</div>");

                //meal course titles
                $("#nav-mealCourse").append(mealCourse);
                $("#nav-mealCourse li:first-child").addClass('active');

                //tabs of meal course
                $("#tab-content-mealCourse").append(mealCourseTab);
                $("#tab-content-mealCourse .tab-pane:first-child").addClass('active');

            });

        }
    });

    // confirms table hash
    $.ajax({
        type: "POST",
        dataType: 'json',
        data: {
            tableHash: tableNumber
        },
        url: "assets/php/tableNumber.php?action=confirmTableHash",
        success: function (data) {

            var params2 = params.toString().length;

            //confirming table hash if valid
            if ((params == 'table=' || params != 'table=' && params2 > 0) && (data == 0)) {
                swal({
                    title: 'Wrong Table',
                    text: "Table doesn't exist!",
                    type: 'warning',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK',
                    timer: 4000,
                    allowOutsideClick: false
                }).then(
                    function () {
                        swal({
                            title: 'Redirecting...',
                            timer: 2000,
                            type: 'info',
                            showConfirmButton: false
                        }).then(
                            function () {
                            },
                            function (dismiss) {
                                if (dismiss === 'timer') {
                                    window.location = 'http://' + domain;
                                }
                            }
                        )
                    }, function (dismiss) {
                        if (dismiss) {
                            swal({
                                title: 'Redirecting...',
                                timer: 2000,
                                type: 'info',
                                showConfirmButton: false
                            }).then(
                                function () {
                                },
                                function (dismiss) {
                                    if (dismiss === 'timer') {
                                        window.location = 'http://' + domain;
                                    }
                                }
                            )
                        }
                    });


            } else if (params == "") { // else if param is empty - display items without ADD TO CART button

                $("li.cart").hide(); // hide cart button

                // get items without ADD TO CART button
                $.ajax({
                    type: "POST",
                    url: "assets/php/products.php?action=getProducts",
                    dataType: 'json',
                    success: function (data) {

                        $.each(data, function (index, value) {

                            var mealCourse = value.mealCourse.replace(/\s+/g, '');

                            var products = $("<div class='col-md-6 col-sm-12 col-xs-12'>" +
                                "<div class='item'>" +
                                "<div class='img'>" +
                                "<img src='admin/" + value.productImage + "' alt='admin/" + value.productImage + "' width='110' height='110' draggable='false'>" +
                                "</div>" +
                                "<div class='details'>" +
                                "<h3><a href='single-item.html?id=" + value.id + "'> " + value.name + " </a></h3>" +
                                "<p>" + value.name + " <br> " + value.description + " <br> </p>" +
                                "<label class='label label-primary'>To be serve in " + value.servingtime + "</label>" +
                                "</div>" +
                                "<div class='price'>" +
                                "<span>₱ " + value.price + "</span>" +
                                "</div>" +
                                "</div>");

                            $("#" + mealCourse).append(products);

                        });
                    }
                });

            } else { // else param is confirmed - display items with ADD TO CART button

                $("li.cart").show(); // show cart button

                // display the items with ADD TO CART button
                $.ajax({
                    type: "POST",
                    url: "assets/php/products.php?action=getProducts",
                    dataType: 'json',
                    success: function (data) {

                        $.each(data, function (index, value) {

                            var mealCourse = value.mealCourse.replace(/\s+/g, '');

                            var products = $("<div class='col-md-6 col-sm-12 col-xs-12'>" +
                                "<div class='item'>" +
                                "<div class='img'>" +
                                "<img src='admin/" + value.productImage + "' alt='admin/" + value.productImage + "' width='110' height='110' draggable='false'>" +
                                "</div>" +
                                "<div class='details'>" +
                                "<h3><a href='single-item.html?id=" + value.id + "&table=" + tableNumber + "'> " + value.name + " </a></h3>" +
                                "<p>" + value.name + " <br> " + value.description + " </p>" +
                                "<label class='label label-primary'>To be serve in " + value.servingtime + "  </label><br><br>" +
                                "<a href='javascript:void(0);' id='" + value.id + "' class='add-cart'>Add To Cart <i class='icon-cart'></i></a>" +
                                "</div>" +
                                "<div class='price'>" +
                                "<span>₱ " + value.price + "</span>" +
                                "</div>" +
                                "</div>");

                            $("#" + mealCourse).append(products);

                            // onClick addToCart
                            $('#' + value.id).click(function () {

                                $.ajax({
                                    type: "POST",
                                    url: "assets/php/addToCart.php?action=getProductQty",
                                    data: {
                                        id: value.id
                                    },
                                    success: function (data) {

                                        var x = 1;
                                        var productQty = data;
                                        var inputOptions = new Array(1);

                                        while (x <= productQty) {
                                            inputOptions.push(x);
                                            x++;
                                        }

                                        swal({
                                            title: value.name,
                                            text: 'Add this item to cart?',
                                            imageUrl: 'admin/' + value.productImage,
                                            imageWidth: 200,
                                            imageHeight: 200,
                                            showCancelButton: true,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'Yes, add it!',
                                            cancelButtonText: 'No, cancel!',
                                            confirmButtonClass: 'btn btn-success',
                                            cancelButtonClass: 'btn btn-danger',
                                            buttonsStyling: false,
                                            background: '#fff url(//bit.ly/1Nqn9HU)',
                                            input: 'select',
                                            inputValidator: function (value) {
                                                return new Promise(function (resolve, reject) {
                                                    if (value > 0) {
                                                        resolve()
                                                    } else {
                                                        reject('You need to select quantity :)')
                                                    }
                                                })
                                            },
                                            inputOptions: inputOptions,
                                            inputPlaceholder: 'Quantity',
                                            allowOutsideClick: false,

                                        }).then(function (result) {

                                            params = new URL(document.URL).searchParams;
                                            var tableNumber = params.get('table');

                                            $.ajax({
                                                type: "POST",
                                                dataType: 'json',
                                                data: {
                                                    id: value.id,
                                                    qty: result,
                                                    tableNum: tableNumber
                                                },
                                                url: "assets/php/addToCart.php?action=addToCart",
                                                success: function (data) {

                                                    // successful add to cart
                                                    swal({
                                                        title: 'Added!',
                                                        text: 'Your item has been added to cart.',
                                                        type: 'success',
                                                        showConfirmButton: false,
                                                        timer: 2000
                                                    })
                                                    $("#cartCount,.cart-count").empty();
                                                    $("#cartCount,.cart-count").append(data);
                                                }
                                            });

                                        }, function (dismiss) {
                                            // cancelled add to cart
                                            if (dismiss === 'cancel') {
                                                swal({
                                                    title: 'Cancelled!',
                                                    text: 'Your selected item has been cancelled.',
                                                    type: 'error'
                                                })
                                            }
                                        })
                                    }
                                });

                            });

                        });
                    }
                });

            }
        }
    });

});

