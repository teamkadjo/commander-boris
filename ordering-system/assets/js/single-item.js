// SINGLE ITEM SCRIPT
// * DISPLAYS SINGLE ITEM
// + CONFIRMS ITEM ID
// + CONFIRMS TABLE HASH

$(function () {

    // domain name
    var domain = window.location.hostname;

    //query string
    params = new URL(document.URL).searchParams;
    var tableNumber = params.get('table');
    var productId = params.get('id');

    // alert for wrong param itemID
    function alertWrongItem() {
        $('.single-item').hide();
        swal({
            title: 'Wrong Item',
            text: "Item doesn't exist!",
            type: 'warning',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            timer: 4000,
            allowOutsideClick: false
        }).then(
            function () {
                swal({
                    title: 'Redirecting...',
                    timer: 2000,
                    type: 'info',
                    showConfirmButton: false
                }).then(
                    function () {
                    },
                    function (dismiss) {
                        if (dismiss === 'timer') {
                            window.location = 'http://' + domain;
                        }
                    }
                )
            }, function (dismiss) {
                if (dismiss) {
                    swal({
                        title: 'Redirecting...',
                        timer: 2000,
                        type: 'info',
                        showConfirmButton: false
                    }).then(
                        function () {
                        },
                        function (dismiss) {
                            if (dismiss === 'timer') {
                                window.location = 'http://' + domain;
                            }
                        }
                    )
                }
            });
    }

    // alert for wrong tableHash
    function alertWrongTable() {
        $('.single-item').hide();
        swal({
            title: 'Wrong Table',
            text: "Table doesn't exist!",
            type: 'warning',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            timer: 4000,
            allowOutsideClick: false
        }).then(
            function () {
                swal({
                    title: 'Redirecting...',
                    timer: 2000,
                    type: 'info',
                    showConfirmButton: false
                }).then(
                    function () {
                    },
                    function (dismiss) {
                        if (dismiss === 'timer') {
                            window.location = 'http://' + domain;
                        }
                    }
                )
            }, function (dismiss) {
                if (dismiss) {
                    swal({
                        title: 'Redirecting...',
                        timer: 2000,
                        type: 'info',
                        showConfirmButton: false
                    }).then(
                        function () {
                        },
                        function (dismiss) {
                            if (dismiss === 'timer') {
                                window.location = 'http://' + domain;
                            }
                        }
                    )
                }
            });
    }

    // displays single item
    $.ajax({
        type: "POST",
        url: "assets/php/products.php?action=singleItem&id=" + productId,
        dataType: 'json',
        success: function (data) {

            // confirms item id
            if (data <= 0) {
                alertWrongItem();
            } else {

                $.each(data, function (index, value) {

                    $("#singleItem-image").attr({
                        alt: value.name,
                        src: 'admin/' + value.productImage,
                        width: "550",
                        height: "400"
                    });

                    $("#singleItem-name").append(value.name);
                    $("#singleItem-description").append(value.description);
                    $("#singleItem-price").append("₱ " + value.price);
                    $("#singleItem_servingTime").append('To be serve in '+value.servingtime);

                    if (tableNumber.toString().length > 0) {
                        $("#addToCart-singleItem").append('<a href="javascript:void(0);" id="' + value.id + '" class="btn btn-common"><i class="icon-cart"></i> Add to cart</a>');
                        $('select[name="qty"]').show();
                    }

                    // onClick addToCart
                    $('#' + value.id).click(function () {

                        $.ajax({
                            type: "POST",
                            url: "assets/php/addToCart.php?action=getProductQty",
                            data: {
                                id: value.id
                            },
                            success: function (data) {

                                var x = 1;
                                var productQty = data;
                                var inputOptions = new Array(1);

                                while (x <= productQty) {
                                    inputOptions.push(x);
                                    x++;
                                }

                                swal({
                                    title: value.name,
                                    text: 'Add this item to cart?',
                                    imageUrl: 'admin/' + value.productImage,
                                    imageWidth: 200,
                                    imageHeight: 200,
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Yes, add it!',
                                    cancelButtonText: 'No, cancel!',
                                    confirmButtonClass: 'btn btn-success',
                                    cancelButtonClass: 'btn btn-danger',
                                    buttonsStyling: false,
                                    background: '#fff url(//bit.ly/1Nqn9HU)',
                                    input: 'select',
                                    inputValidator: function (value) {
                                        return new Promise(function (resolve, reject) {
                                            if (value > 0) {
                                                resolve()
                                            } else {
                                                reject('You need to select quantity :)')
                                            }
                                        })
                                    },
                                    inputOptions: inputOptions,
                                    inputPlaceholder: 'Quantity',
                                    allowOutsideClick: false,

                                }).then(function (result) {

                                    params = new URL(document.URL).searchParams;
                                    var tableNumber = params.get('table');

                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        data: {
                                            id: value.id,
                                            qty: result,
                                            tableNum: tableNumber
                                        },
                                        url: "assets/php/addToCart.php?action=addToCart",
                                        success: function (data) {

                                            // successful add to cart
                                            swal({
                                                title: 'Added!',
                                                text: 'Your item has been added to cart.',
                                                type: 'success',
                                                showConfirmButton: false,
                                                timer: 2000
                                            })
                                            $("#cartCount").empty();
                                            $("#cartCount").append(data);
                                        }
                                    });

                                }, function (dismiss) {
                                    // cancelled add to cart
                                    if (dismiss === 'cancel') {
                                        swal({
                                            title: 'Cancelled!',
                                            text: 'Your selected item has been cancelled.',
                                            type: 'error'
                                        })
                                    }
                                })
                            }
                        });

                    });

                });
            }
        }
    });

    //confirms table hash
    $.ajax({
        type: "POST",
        dataType: 'json',
        data: {
            tableHash: tableNumber
        },
        url: "assets/php/tableNumber.php?action=confirmTableHash",
        success: function (data) {
            if (tableNumber.toString().length > 0 && data == 0) {
                alertWrongTable();
            } else {
                if (data == 0) {
                    $('select[name="qty"]').hide();
                    $("#addToCart-singleItem").hide();
                }
            }

        }
    });

});

