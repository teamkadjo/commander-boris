$(function () {

    //query string
    params = new URL(document.URL).searchParams;
    var tableNumber = params.get('table');

    // get the mealCourse
    $.ajax({
        type: "POST",
        url: "assets/php/products.php?action=getMealCourse",
        dataType: 'json',
        success: function (data) {

            $.each(data, function (index, value) {
                var mealCouse = value.mealCourse.replace(/\s+/g, '');

                var mealCourse = $("<li>" +
                    "<a data-toggle='tab' role='tab' href='#" + mealCouse + "-tab'> " + value.mealCourse + " </a>" +
                    "</li>");

                var mealCourseTab = $("<div id='" + mealCouse + "-tab' class='tab-pane fade in' role='tabpanel'>" +
                    "<div class='row' id='" + mealCouse + "'></div>" +
                    "</div>");

                //meal course titles
                $("#nav-mealCourse").append(mealCourse);
                $("#nav-mealCourse li:first-child").addClass('active');

                //tabs of meal course
                $("#tab-content-mealCourse").append(mealCourseTab);
                $("#tab-content-mealCourse .tab-pane:first-child").addClass('active');

            });

        }
    });

    // display the products
    $.ajax({
        type: "POST",
        url: "assets/php/products.php?action=getProducts",
        dataType: 'json',
        success: function (data) {

            $.each(data, function (index, value) {

                var mealCourse = value.mealCourse.replace(/\s+/g, '');

                var products = $("<div class='col-md-6 col-sm-12 col-xs-12'>" +
                    "<div class='item'>" +
                    "<div class='img'>" +
                    "<img src='admin/" + value.productImage + "' alt='admin/" + value.productImage + "' width='110' height='110'>" +
                    "</div>" +
                    "<div class='details'>" +
                    "<h3><a href='single-item.html?id=" + value.id + "&table" + tableNumber + "'> " + value.name + " </a></h3>" +
                    "<p>" + value.name + " <br> " + value.description + " </p>" +
                    "<a href='javascript:void(0);' id='" + value.id + "' class='add-cart'>Add To Cart <i class='icon-cart'></i></a>" +
                    "</div>" +
                    "<div class='price'>" +
                    "<span>" + value.price + "</span>" +
                    "</div>" +
                    "</div>");

                $("#" + mealCourse).append(products);

                // onClick addToCart
                $('#' + value.id).click(function () {

                    //get item details and addToCart
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        data: {
                            id: value.id,
                        },
                        url: "assets/php/addToCart.php?action=getProductQty",
                        success: function (data) {

                            swal({
                                title: value.name,
                                text: 'Add this item to cart?',
                                imageUrl: 'admin/' + value.productImage,
                                imageWidth: 200,
                                imageHeight: 200,
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes, add it!',
                                cancelButtonText: 'No, cancel!',
                                confirmButtonClass: 'btn btn-success',
                                cancelButtonClass: 'btn btn-danger',
                                buttonsStyling: false,
                                background: '#fff url(//bit.ly/1Nqn9HU)',
                                input: 'select',
                                inputValidator: function (value) {
                                    return new Promise(function (resolve, reject) {
                                        if (value > 0) {
                                            resolve()
                                        } else {
                                            reject('You need to select quantity :)')
                                        }
                                    })
                                },
                                inputOptions: {
                                    '1': '1',
                                    '2': '2',
                                    '3': '3',
                                    '4': '4',
                                    '5': '5'
                                },
                                inputPlaceholder: 'Quantity',
                                allowOutsideClick: false,

                            }).then(function (result) {

                                params = new URL(document.URL).searchParams;
                                var tableNumber = params.get('table');

                                $.ajax({
                                    type: "POST",
                                    dataType: 'json',
                                    data: {
                                        id: value.id,
                                        qty: result,
                                        tableNum: tableNumber
                                    },
                                    url: "assets/php/addToCart.php?action=addToCart",
                                    success: function (data) {
                                        swal({
                                            title: 'Added!',
                                            text: 'Your item has been added to cart.',
                                            type: 'success',
                                        })
                                        $("#cartCount").empty();
                                        $("#cartCount").append(data);
                                    }
                                });

                            }, function (dismiss) {
                                if (dismiss === 'cancel') {
                                    swal({
                                        title: 'Cancelled!',
                                        text: 'Your selected item has been cancelled.',
                                        type: 'error'
                                    })
                                }
                            })

                        }
                    });

                });

            });
        }
    });

});