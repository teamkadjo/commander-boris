<?php
require_once 'DBconfig.php';

function getProducts()
{
    global $db_con;

    try {
        $stmt = $db_con->query("SELECT * FROM products WHERE status = 'active' && quantity != 0");
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($row);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

}

function singleItem()
{
    global $db_con;

    $productID = $_REQUEST['id'];

    try {
        $stmt = $db_con->query("SELECT * FROM products WHERE id = $productID && status = 'active'");
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($row);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

}

function getMealCourse()
{
    global $db_con;

    try {
        $stmt = $db_con->query("SELECT mealCourse FROM products WHERE status = 'active' && quantity != 0 GROUP BY mealCourse");
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($row);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

}

$action = $_REQUEST['action'];
if ($action == 'getProducts') {
    getProducts();
} else if ($action == 'getMealCourse') {
    getMealCourse();
} else if ($action == 'singleItem') {
    singleItem();
}
