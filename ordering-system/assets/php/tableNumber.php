<?php
require_once 'DBconfig.php';

function tableHash()
{
    global $db_con;

    try {
        $stmt = $db_con->query("SELECT * FROM tables");
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($row);

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function confirmTableHash()
{
    global $db_con;
    $tableHash = $_REQUEST['tableHash'];

    if (strlen($tableHash) <= 0) {
        $tableHash = 0;
    }

    try {
        $stmt = $db_con->query("SELECT * FROM tables WHERE hash = '$tableHash'");
        $row = $stmt->rowCount();
        echo json_encode($row);

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

$action = $_REQUEST['action'];
if ($action == 'getTableNumber') {
    tableHash();
} else if ($action == 'confirmTableHash') {
    confirmTableHash();
}




