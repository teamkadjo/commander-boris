<?php
require_once 'DBconfig.php';

function getProducts()
{
    global $db_con;

    try {
        $stmt = $db_con->query("SELECT * FROM products");
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($row);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

}

function getUsers()
{
    global $db_con;

    try {
        $stmt = $db_con->query("SELECT * FROM users");
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($row);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

$action = $_REQUEST['action'];
if ($action == 'getProducts') {
    getProducts();
} else if ($action == 'getUsers') {
    getUsers();
}
