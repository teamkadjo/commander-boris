<?php
class DBConnection{
	private $db;

		/*
		Connects to the Specified Database when Class is initialized
		*/
	   function __construct($dbname) {
	      
	      try{
			$host = '127.0.0.1';
			$db   = $dbname;
			$user = 'ordering';
			$pass = 'ordering';
			$charset = 'utf8';

			$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
			$opt = [
			    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			    PDO::ATTR_EMULATE_PREPARES   => false,
			];

			$PDOinstance = new PDO($dsn, $user, $pass, $opt);
			$this->db = $PDOinstance;
			}catch(PDOException $ex){

				echo "There was a problem with your connection! <br> Error Message is: (". $ex->getMessage() . ") ";

			}	
	   }
	   /*
		Returns a JSON Encoded Array of Results
	   */
	   function Select_Query($querystring){
	   		if($this->db != null){
		   		$conn = $this->db;
		   		$stmt = $conn->query($querystring);
		   		$rows = $stmt->fetchAll();
		   		return json_encode($rows);
	   		}

   	   }

}


?>